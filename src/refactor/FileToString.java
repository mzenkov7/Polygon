package refactor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileToString {

    // Java 6 style
    String readFile(String fileName, String encoding) {
        StringBuilder out = new StringBuilder();
        char buf[] = new char[1024];
        InputStream inputStream = null;
        Reader reader = null;
        try {
            inputStream = new FileInputStream(fileName);
            reader = new InputStreamReader(inputStream, encoding);
            for (int i = reader.read(buf); i >= 0; i = reader.read(buf)) {
                out.append(buf, 0, i);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        String result = out.toString();
        return result;
    }

    // Java 7 style - try with resources
    String readFileTryWithResources(String fileName, String encoding) {
        StringBuilder out = new StringBuilder();
        char buf[] = new char[1024];
        try (InputStream inputStream = new FileInputStream(fileName);
                Reader reader = new InputStreamReader(inputStream, encoding)) {
            for (int i = reader.read(buf); i >= 0; i = reader.read(buf)) {
                out.append(buf, 0, i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toString();
    }

    // Java 7 NIO
    String readFileWithNIO(String fileName, String encoding) {
        String s = null;
        try {
            s = new String(Files.readAllBytes(Paths.get(fileName)), encoding);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }
}
