package bad;

public class StaticLoad {

    public static void main(String[] args) {
        System.out.print(C.x);
        // AB5

        // Access to C class variable
        // that declared in parent - class B
        // but class C doesn't load
    }
}

class A {
    static {
        System.out.print("A");
    }
}

class B extends A {
    static int x = 5;
    static {
        System.out.print("B");
    }
}

class C extends B {
    static {
        System.out.print("C");
        System.exit(0);
    }
}
