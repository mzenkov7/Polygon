package bad;

import java.util.Random;

public class Ternal {

    public static void main(String[] args) {
        boolean flag = new Random().nextBoolean();

        autoBoxing(flag);
        nullInTernar(flag);
    }

    private static void autoBoxing(boolean flag) {
        Object o1 = null;
        if (flag) {
            o1 = new Integer(1);
        } else {
            o1 = new Double(2.0);
        }

        Object o2 = (flag) ? new Integer(1) : new Double(2.0);

        System.out.println(o1);
        System.out.println(o2);

        // if flag = false then
        //      2.0
        //      2.0
        // but if flag = true
        //      1
        //      1.0
    }

    private static void nullInTernar(boolean flag) {

        // Compiling
        int i = flag ? 4 : null;

        System.out.println(i);

        // if flag = true then
        //      4
        // but if flag = false
        //      NullPointerException
    }
}
