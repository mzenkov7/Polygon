package bad;

/**
 * How to write method with not void return type <br>
 * and without return statement.
 */
public class WithoutReturn {

    public double infiniteLoop() {
        while (true)
            ;
    }

    public double throwExcetion() {
        throw new RuntimeException();
    }

    public double combination() {
        if (Math.random() > 0.5) {
            while (true)
                ;
        } else {
            throw new RuntimeException();
        }
    }
}
