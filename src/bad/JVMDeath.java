package bad;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class JVMDeath {
    public static void main(final String[] args) throws Exception {
        final Method badMethod = JVMDeath.class.getDeclaredMethod("returnInteger", int.class);
        doBad(badMethod);

        System.out.println(badMethod.invoke(null, 0));
    }

    public static int returnInteger(final int i) {
        return Integer.MAX_VALUE;
    }

    private static void doBad(final Method declaredMethod) throws Exception {
        final Field declaredField = declaredMethod.getClass().getDeclaredField("returnType");
        declaredField.setAccessible(true);
        // change return type to null
        declaredField.set(declaredMethod, null);
    }
}
