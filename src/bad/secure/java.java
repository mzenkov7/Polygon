package bad.secure;

import java.security.Permission;

public class java {
    public static class lang {
        public static class Object {

            static {
                DoNotTerminate.forbidExit();
            }

            private static class DoNotTerminate {

                private static void forbidExit() {
                    final SecurityManager securityManager = new SecurityManager() {

                        @Override
                        public void checkPermission(Permission permission) {
                            // prevent System.exit();
                            if (permission.getName().contains("exitVM")) {
                                throw new ExitTrappedException();
                            }
                            // prevent changing of current SM
                            if (permission.getName().equals("setSecurityManager")) {
                                throw new RuntimeException("Fail.");
                            }
                        }
                    };
                    System.setSecurityManager(securityManager);
                }
            }

            private static class ExitTrappedException extends SecurityException {
                private static final long serialVersionUID = 1;
            }
        }
    }
}
