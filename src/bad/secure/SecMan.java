package bad.secure;

// java.lang.Object - own class, just for distraction
public class SecMan extends java.lang.Object {

    public static void main(String[] args) {
        try {
            System.out.println("Before");
            System.exit(0);
        } catch (Exception e) {
            System.out.println("I don't care.");
        } finally {
            System.out.println("Finally");
        }
        System.out.println("Life after System.exit(0)");
    }
    // Before
    // I don't care.
    // Finally
    // Life after System.exit(0)
}
