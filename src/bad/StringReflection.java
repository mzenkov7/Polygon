package bad;

import java.lang.reflect.Field;

public class StringReflection {

    public static void main(String[] args) throws SecurityException, Exception {
        String s = "Java";
        System.out.println(s);      // "Java"
        System.out.println("Java"); // "Java"

        System.out.println();
        changeValueProperty(s);

        System.out.println(s);      // Print object: "Not java"
        System.out.println("Java"); // Print literal: "Not java"
    }

    private static void changeValueProperty(String s) throws Exception, SecurityException {
        Field field = s.getClass().getDeclaredField("value");
        field.setAccessible(true);
        field.set(s, "Not java".toCharArray());
    }
}
