package bad;

public class NullStatic {
    public static void main(String[] args) {

        // Static is accessible by null reference
        ((NullStatic) null).hi();

        // Inner class not accessible
        // NullPointerException
        ((NullStatic) null).new Z().hi();
    }

    class Z {
        private void hi() {
            System.out.println("Hi!");
        }
    }

    private static void hi() {
        System.out.println("Hi!");
    }
}
