package bad;

import java.util.Arrays;

public class BadSyntax {;;

    ;;;

    public static void main(String[] args) {

        Byte[] Byte[] = { { 0 } };
        System.out.println(Byte);
        System.out.println(Byte.class);
        System.out.println(Byte.length);
        System.out.println(new Byte("10"));

        int[][] mas = { { 1, 2 }, { 2, 3 }, };
        System.out.println(Arrays.deepToString(mas));

    }

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;public void f() {
    }

}
