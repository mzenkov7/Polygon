package bad;

public class Cast {

    public static void main(String[] args) {

        // Some awful syntax

        int i = (byte) + (char) - (int) + (long) - 1;
        System.out.println(i);
        // 1

        System.out.println((int) (char) - 1);
        // 65535

        System.out.println(-1);
        // -1
        System.out.println(+ (long) - 1);
        // -1
        System.out.println(- (int) + (long) - 1);
        // 1
        System.out.println(+ (char) - (int) + (long) - 1);
        // 1
        System.out.println((byte) + (char) - (int) + (long) - 1);
        // 1
    }
}
