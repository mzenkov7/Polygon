package bad;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class AnnotationImplements {

    public static class Inner {

        // Annotation still is interface
        enum Dakh implements MyAnn {
            ONE("Dres"), TWO("Vkol");

            private String message;

            Dakh(String mes) {
                this.message = mes;
            }

            @Override
            public Class<? extends Annotation> annotationType() {
                return null;
            }

            @Override
            public String getMessage() {
                return message;
            }
        }
    }

    public static void main(String[] args) {

        // Bad but correct syntax
        System.out.println(bad.AnnotationImplements.Inner.Dakh.ONE.getMessage());
        System.out.println(bad.AnnotationImplements.Inner.Dakh.TWO.getMessage());

        System.out.println(new MyAnn() {

            @Override
            public Class<? extends Annotation> annotationType() {
                return MyAnn.class;
            }

            @Override
            public String getMessage() {
                return "terh";
            }

        }.getMessage());
    }

}

// @FunctionalInterface
// already have one method "annotationType()"
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface MyAnn {

    String getMessage() default "";
}
