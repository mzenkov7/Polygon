package bad;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ChangeReturnType {
    public static void main(final String[] args) throws Exception {
        final Method intToLong = ChangeReturnType.class.getDeclaredMethod("returnInteger", int.class);
        makeLong(intToLong);

        System.out.println(Integer.MAX_VALUE);                      // 2147483647
        System.out.println(intToLong.invoke(null, 0));              // 2147483748
        System.out.println(intToLong.invoke(null, 0).getClass());   // class java.lang.Long
    }

    public static int returnInteger(final int i) {
        return Integer.MAX_VALUE + 101;
    }

    private static void makeLong(final Method declaredMethod) throws Exception {
        final Field declaredField = declaredMethod.getClass().getDeclaredField("returnType");
        declaredField.setAccessible(true);
        declaredField.set(declaredMethod, long.class);
    }
}
