package bad;

import java.util.concurrent.TimeUnit;

public class SynchString extends Thread {

    public static void main(String[] argv) throws Exception {
        Thread t1 = new SynchString("Thread A");
        Thread t2 = new SynchString("Thread B");
        t1.start();
        t2.start();
        TimeUnit.MILLISECONDS.sleep(2000);
        t1.interrupt();
        t2.interrupt();
    }

    private String name;

    public SynchString(String mes) {
        this.name = mes;
    }

    public void run() {
        // Java string literal is an Object, so
        synchronized ("") {
            try {
                while (!isInterrupted()) {
                    "".notify();
                    "".wait();
                    TimeUnit.MILLISECONDS.sleep(100);
                    System.out.println(this.name);
                }
            } catch (InterruptedException e) {
                System.out.println(name + " has been interapted");
            }
        }
    }
}
