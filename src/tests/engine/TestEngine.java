package tests.engine;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tests.engine.MethodSummary.MethodResult;

/**
 * Test engine for testing java code. <br>
 * Test class have to provide constructor without parameters. <br>
 * Test methods have to be public, without parameters <br>
 * and annotated with {@link Test} annotation.
 */
public class TestEngine {

    /** Header and footer delimiter. */
    private static final String LINE = "---------------------------------------------";

    /**
     * Test all valid test methods of given classes. <br>
     * Print summary test information for each class.
     * 
     * @param clazzes
     */
    public void test(Class<?>... clazzes) {
        for (Class<?> clazz : clazzes) {
            test(clazz);
        }
    }

    /**
     * Test all valid test methods of given class. <br>
     * Print summary test info.
     * 
     * @param clazz
     *            class with methods to be tested
     */
    public void test(Class<?> clazz) {
        printHeader(clazz);

        Method[] methods = clazz.getMethods();
        List<MethodSummary> methodSummaries = new ArrayList<>();

        for (Method method : methods) {
            if (method.isAnnotationPresent(Test.class)) {
                MethodSummary summary = methodInfo(clazz, method);
                methodSummaries.add(summary);
            }
        }
        Collections.sort(methodSummaries, (m1, m2) -> m1.getResult().compareTo(m2.getResult()));
        ClassSummary data = new ClassSummary(methodSummaries);
        printSummary(data);
    }

    /**
     * Invoke tested method and analyze result of his execution.
     * 
     * @param clazz
     *            tested class
     * @param method
     *            tested method
     * @return summary information of method execution as instance of
     *         {@link MethodSummary}
     */
    private MethodSummary methodInfo(Class<?> clazz, Method method) {
        Test annotation = method.getAnnotation(Test.class);
        Class<? extends Throwable> expected = annotation.expected();
        MethodResult result = MethodResult.PASS;
        try {
            Object tested = getInstance(clazz);
            method.invoke(tested);
            return new MethodSummary(method);
        } catch (IllegalAccessException | IllegalArgumentException e) {
            throw new RuntimeException("Invoke method exception", e);
        } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause.getClass() == expected) {
                result = MethodResult.PASS_EXC;
            } else if (cause.getClass() == AssertionError.class) {
                result = MethodResult.FAIL;
            } else {
                result = MethodResult.ERROR;
            }
            return new MethodSummary(method, result, cause);
        }
    }

    /**
     * Return instance of tested class. <br>
     * If class does not provide public constructor without parameters - throw
     * Exception.
     * 
     * @param clazz
     *            tested class
     * @return instance of tested class
     */
    private Object getInstance(Class<?> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("Instantiation error", e);
        }
    }

    /**
     * Print summary class info header.
     * 
     * @param clazz
     *            tested class
     */
    private void printHeader(Class<?> clazz) {
        System.out.println(LINE);
        System.out.println("Tested class: " + clazz.getName());
        System.out.println(LINE);
    }

    /**
     * Print summary class info footer.
     * 
     * @param data
     *            result of testing class methods
     */
    private void printSummary(ClassSummary data) {
        System.out.println(data.getMethodSummaryData());
        System.out.println(LINE);
        System.out.println(data);
        System.out.println(LINE);
    }
}
