package tests.engine;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation marker for {@link TestEngine}. <br>
 * Should be used for public method without parameters.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Test {

    /**
     * If method should throw exception, this exception have to be in expected
     * parameter field.
     * 
     * @return expected exception of method execution
     */
    Class<? extends Throwable> expected() default None.class;

    class None extends Throwable {
        private static final long serialVersionUID = 1L;
    }
}
