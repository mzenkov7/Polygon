package tests.engine;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import tests.engine.MethodSummary.MethodResult;

/** Represent tested method result of tested class. */
class ClassSummary {
    private static final String SUMMARY_PATTERN = "Runs: %d | passed %d | failures %d | errors: %d";

    private final String methodsSummaryData;
    private final String testEngineSummaryData;
    private final List<MethodSummary> emptyList = Collections.emptyList();

    /**
     * Create instance with given parameters.
     * 
     * @param methodSummaries
     *            tested method of tested class.
     */
    ClassSummary(List<MethodSummary> methodSummaries) {
        Objects.requireNonNull(methodSummaries);

        this.methodsSummaryData = methodSummaries.stream().map(m -> m.toString()).collect(Collectors.joining("\n"));
        int runs = methodSummaries.size();

        Map<MethodResult, List<MethodSummary>> map;
        map = methodSummaries.stream().collect(Collectors.groupingBy(MethodSummary::getResult));

        int successes = map.getOrDefault(MethodResult.PASS, emptyList).size();
        successes += map.getOrDefault(MethodResult.PASS_EXC, emptyList).size();
        int failures = map.getOrDefault(MethodResult.FAIL, emptyList).size();
        int errors = map.getOrDefault(MethodResult.ERROR, emptyList).size();

        this.testEngineSummaryData = String.format(SUMMARY_PATTERN, runs, successes, failures, errors);
    }

    public String getMethodSummaryData() {
        return methodsSummaryData;
    }

    @Override
    public String toString() {
        return testEngineSummaryData;
    }

}