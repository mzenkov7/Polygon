package tests.engine;

import java.lang.reflect.Method;

/**
 * Represent result of tested method execution. <br>
 * Provide different types of method completion result.
 */
class MethodSummary {

    /**
     * Representation of method completion result.
     */
    enum MethodResult {

        /** Successful completion without exception. */
        PASS {
            String getInfo(Throwable cause) {
                return "Ok";
            }
        },

        /** Successful completion with throwing of expected exception. */
        PASS_EXC {
            String getInfo(Throwable cause) {
                return "As expected - " + cause;
            }
        },

        /** Unsuccessful completion based of wrong obtained results. */
        FAIL {
            String getInfo(Throwable cause) {
                return "Assertion error - " + cause.getMessage();
            }
        },

        /** Unsuccessful completion with throwing of unexpected exception. */
        ERROR {
            String getInfo(Throwable cause) {
                return "Not expected - " + cause;
            }
        };

        /**
         * Processing of method execution result using completion result type.
         * 
         * @param cause
         *            actual exception
         * @return String representation of method execution result
         */
        abstract String getInfo(Throwable cause);
    }

    private static final String pattern = "%s(): %s";
    private final String methodInfo;
    private final MethodResult result;

    /**
     * Create instance with given parameters. <br>
     * Using with successful method without expected exception.
     * 
     * @param method
     *            tested method
     */
    public MethodSummary(Method method) {
        this(method, MethodResult.PASS, null);
    }

    /**
     * Create instance with given parameters.
     * 
     * @param method
     *            tested method
     * @param result
     *            result type of method
     * @param cause
     *            actual exception cause
     */
    public MethodSummary(Method method, MethodResult result, Throwable cause) {
        this.result = result;
        String methodName = method.getName();
        String info = result.getInfo(cause);
        this.methodInfo = String.format(pattern, methodName, info);
    }

    public MethodResult getResult() {
        return result;
    }

    @Override
    public String toString() {
        return methodInfo;
    }
}
