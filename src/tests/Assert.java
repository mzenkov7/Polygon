package tests;

import java.util.Objects;

public class Assert {

    public static void assertTrue(boolean condition) {
        assertEquals(true, condition);
    }

    public static void assertFalse(boolean condition) {
        assertEquals(false, condition);
    }

    public static void assertEquals(Object expected, Object actual) {
        if (!Objects.equals(expected, actual)) {
            fail(String.format("Expected: <%s> but actual: <%s>", expected, actual));
        }
    }

    public static void assertNotEquals(Object expected, Object actual) {
        if (!Objects.equals(expected, actual)) {
            fail("Values should be different");
        }
    }

    public static void fail(String message) {
        if (message == null) {
            throw new AssertionError();
        }
        throw new AssertionError(message);
    }
}
