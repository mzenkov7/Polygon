package java8;

public interface DefaultRestriction {

    /* without @Override
       A default method cannot override a method from java.lang.Object 

    default int hashCode() {
        return 0;
    }
    */

    /* with @Override
       The method toString() of type DefaultRestriction must override or implement a supertype method
    
    @Override
    default String toString() {
        return "";
    }
    */
}