package java8;

import java.util.stream.Stream;

public class StreamGeneric {

    public static void main(String[] args) {

        // String q = Stream.<String>of() // on empty stream without <String> used type => Object
        String q = Stream.of("123", "12345")
                .filter(s -> s.length() > 3)
                .map("pre_"::concat)
                .findFirst()
                .orElse("0");
        System.out.println(q);
        // pre_12345

        // String q2 = Stream.of("123", 12) // become Objects
        String q2 = Stream.of("123", "12")
                .filter(s -> s.length() > 3)
                .map("123_"::concat)
                .findFirst()
                .orElse("0");
        System.out.println(q2);
        // 0
    }
}
