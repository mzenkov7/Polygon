package java8;

import java.util.function.Function;

public class FunctionalTest {

    public static void main(String[] args) {
        Converter<String, Double> converterReference   = Double::valueOf;
        Function <String, Double> converterConstructor = Double::new;
        Converter<String, Double> converterLambda      = (param) -> Double.valueOf(param);
        Function <String, Double> converterFunction    = (param) -> Double.valueOf(param);

        // 123.0
        System.out.println(converterReference.convert("123"));
        System.out.println(converterConstructor.apply("123"));
        System.out.println(converterLambda.convert("123"));
        System.out.println(converterFunction.apply("123"));
    }
}

@FunctionalInterface
interface Converter<F, T> {
    T convert(F from);
}