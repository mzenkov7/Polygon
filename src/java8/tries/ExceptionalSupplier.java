package java8.tries;

/**
 * Supplier that can throw exception.
 *
 * @param <T>
 *            the type of results supplied by this supplier
 */
@FunctionalInterface
interface ExceptionalSupplier<T> {

    /**
     * Gets a result.
     * 
     * @return a result
     * @throws Exception
     *             if occurred
     */
    T get() throws Exception;
}