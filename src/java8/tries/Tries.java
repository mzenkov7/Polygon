package java8.tries;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Try catch implementation by Java 8 with Lambda. <br>
 * Take action to execute and exceptions to handle. <br>
 * Take care of given exception by given handler action.
 *
 * @param <T>
 *            - type of return value
 */
public class Tries<T> {

    /**
     * Exception check mode. <br>
     * Define which exceptions are allowed.
     */
    public enum Mode {

        /**
         * Exception valid if it's instance or child of one handled exception.
         */
        INHERITANCE {
            boolean test(Set<Class<? extends Exception>> set, Exception tested) {
                for (Class<? extends Exception> e : set) {
                    if (e.isInstance(tested))
                        return true;
                }
                return false;
            }
        },

        /**
         * Exception valid if it's instance of one handled exception.
         */
        EQUALS {
            boolean test(Set<Class<? extends Exception>> set, Exception tested) {
                for (Class<? extends Exception> e : set) {
                    if (tested.getClass().equals(e))
                        return true;
                }
                return false;
            }
        };

        /**
         * Check if given exception is expected.
         * 
         * @param set
         *            expected exceptions
         * @param tested
         *            exception to be tested
         * @return true if exception is expected, otherwise - false
         */
        abstract boolean test(Set<Class<? extends Exception>> set, Exception tested);
    }

    /**
     * Given action to execute.
     */
    private ExceptionalSupplier<T> action;
    /**
     * Expected exceptions.
     */
    private Set<Class<? extends Exception>> exceptionClasses = new HashSet<>();
    /**
     * Handler for expected exception.
     */
    private Consumer<Exception> exceptionHandler;
    /**
     * Exception check mode. {@link Mode#INHERITANCE} by default.
     */
    private Mode mode = Mode.INHERITANCE;
    /**
     * Prevent re-execution.
     */
    private boolean wasRunned;

    /**
     * Set up exception check mode.
     * 
     * @param mode
     *            exception check mode
     * @return current {@link Tries} object
     */
    public Tries<T> mode(Mode mode) {
        Objects.requireNonNull(mode);
        this.mode = mode;
        return this;
    }

    /**
     * Take action to be executed.
     * 
     * @param action
     *            given action
     * @return current {@link Tries} object
     */
    public Tries<T> tries(ExceptionalSupplier<T> action) {
        Objects.requireNonNull(action);
        this.action = action;
        return this;
    }

    /**
     * Take exception to be handled.
     * 
     * @param clazz
     *            given exception class
     * @return current {@link Tries} object
     */
    public Tries<T> when(Class<? extends Exception> clazz) {
        Objects.requireNonNull(clazz);
        this.exceptionClasses.add(clazz);
        return this;
    }

    /**
     * Set up exception handler.
     * 
     * @param exceptionHandler
     *            given exception handler
     * @return current {@link Tries} object
     */
    public Tries<T> then(Consumer<Exception> exceptionHandler) {
        Objects.requireNonNull(exceptionHandler);
        this.exceptionHandler = exceptionHandler;
        return this;
    }

    /**
     * Execute given action. <br>
     * Take care of expected exceptions.
     * 
     * @return result of action execution
     */
    public T run() {
        checkState();
        wasRunned = true;
        T t = null;
        try {
            t = (T) action.get();
        } catch (Exception e) {
            if (isExpected(e)) {
                exceptionHandler.accept(e);
            } else {
                throw new RuntimeException("Unexpected exception", e);
            }
        }
        return t;
    }

    /**
     * Check if exception is expected.
     * 
     * @param exc
     *            catched exception
     * @return is exception expected
     */
    private boolean isExpected(Exception exc) {
        return mode.test(exceptionClasses, exc);
    }

    /**
     * Check parameter before start action execution. <br>
     * Re-execution is not allowed.
     */
    private void checkState() {
        if (wasRunned) {
            throw new IllegalStateException("It has already been runned");
        }
        if (Objects.isNull(exceptionHandler) || Objects.isNull(action) || Objects.isNull(mode)
                || exceptionClasses.isEmpty()) {
            throw new IllegalStateException("Instance isn't fully initialized");
        }
    }
}