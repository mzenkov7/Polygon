package java8;

public class DefaultMethodCall {

    public static void main(String[] args) {

        // Looking for method in DefaultMethodCall class
        // Formula formula = (a) -> sqrt(a * 100);

        // Ok
        Formula formula = new Formula() {
            public double calculate(int a) {
                return sqrt(a * 100);
            }
        };

        System.out.println(formula.calculate(25));
    }
}

@FunctionalInterface
interface Formula {
    double calculate(int a);

    default double sqrt(int a) {
        return Math.sqrt(a);
    }
}