package java8;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapCount {
    private static final List<String> list = Arrays.asList(
            "John", "John", "John", "John", "John",
            "Billy", "Billy", "Billy",
            "Antony", "Antony", "Antony", "Antony");

    public static void main(String[] args) {
        System.out.println(java7(list));
        System.out.println(java8MapCompute(list));
        System.out.println(java8Stream(list));
    }

    private static Object java7(List<String> list) {
        Map<String, Integer> map = new HashMap<>();
        for (String name : list) {
            Integer current = map.get(name);
            if (current == null) {
                map.put(name, 1);
            } else {
                map.put(name, current + 1);
            }
        }
        return map;
    }

    private static Object java8MapCompute(List<String> list) {
        Map<String, Integer> map = new HashMap<>();
        list.forEach(name -> {
            map.compute(name, (k, v) -> (v == null) ? 1 : v + 1);
        });
        return map;
    }

    private static Object java8Stream(List<String> list) {
        return list.stream().collect(groupingBy(s -> s, counting()));
    }
}
