package java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Reduce {

    public static void main(String[] args) {
        List<Integer> collection = Arrays.asList(1, 2, 3, 5, 4, 2);

        int sum = collection.stream().reduce((s1, s2) -> s1 + s2).orElse(0);
        System.out.println(sum);

        // only for IntStream
        int sum2 = collection.stream().mapToInt(s -> s).sum();
        System.out.println(sum2);

        // filtered
        int sum3 = collection.stream().filter(o -> o % 2 != 0).reduce((s1, s2) -> s1 + s2).orElse(0);
        System.out.println(sum3);

        // Same result
        int max = collection.stream().reduce(Integer::max).orElse(-1);
        System.out.println(max);
        int max2 = collection.stream().max(Integer::compareTo).orElse(-1);
        System.out.println(max2);

        // On empty stream return -1
        int max3 = IntStream.of().max().orElse(-1);
        System.out.println(max3);
    }
}
