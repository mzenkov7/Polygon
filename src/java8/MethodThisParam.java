package java8;

public class MethodThisParam {

    // since java 1.8 is legal to write like this
    public void action(MethodThisParam this) {
    }

    public static void main(String[] args) {
        MethodThisParam test = new MethodThisParam();
        test.action();

        // The method action() in the type MethodThisParam is not applicable for the arguments (MethodThisParam)
        // test.action(test);
    }
}
