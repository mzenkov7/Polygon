package java8.jpoint.puzzlers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Example1 {

    public static void main(String[] args) {
        test();
        test2();
    }

    private static void test() {
        List<String> list = new ArrayList<>();
        list.add("молоко");
        list.add("хлеб");
        list.add("колбаса");
        Stream<String> stream = list.stream();
        list.add("яйца, яйца ещё!");
        stream.forEach(System.out::println);
    }

    private static void test2() {
        List<String> list = new ArrayList<>();
        list.add("молоко");
        list.add("хлеб");
        list.add("колбаса");
        list = list.subList(0, 2);
        Stream<String> stream = list.stream();
        list.add("яйца, яйца ещё!");
        stream.forEach(System.out::println);
    }
}
