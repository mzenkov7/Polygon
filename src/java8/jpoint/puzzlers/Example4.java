package java8.jpoint.puzzlers;

import java.util.HashMap;
import java.util.Map;

public class Example4 {

    public static void main(String[] args) {
        Map<String, String> old = new HashMap<>();
        old.put("buildTool", "maven");
        old.put("lang", "java");
        old.put("IOC", "jee");
        System.out.println(old);

        Map<String, String> current = new HashMap<>();
        current.put("buildTool", "gradle");
        current.put("lang", "groovy");
        current.put("IOC", "spring");
        System.out.println(current);

        old.replaceAll(current::put);

        System.out.println(old);
        System.out.println(current);
    }
}
