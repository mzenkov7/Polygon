package java8.jpoint.puzzlers;

import java.util.Optional;

public class Example6 {

    public static void main(String[] args) {
        class CatDog implements Cat, Dog {
        }
        // test(new CatDog());
        // test2(new CatDog());
        test3(new CatDog());
    }

    // private static void test(Object obj) {
    //     Cat & Dog x = (Cat & Dog) obj;
    //     x.meow();
    //     x.bark();
    // }

    // private static void test2(Object obj) {
    //     ( (Consumer<? extends Cat & Dog>) (x -> {
    //         x.meow();
    //         x.bark();
    //     })).accept((Cat & Dog)obj);
    // }

    private static void test3(Object obj) {
        Optional.of((Cat & Dog) obj).ifPresent(x -> {
            x.meow();
            x.bark();
        });
    }
}

interface Cat {
    default void meow() {
        System.out.println("meow");
    }
}

interface Dog {
    default void bark() {
        System.out.println("bark");
    }
}