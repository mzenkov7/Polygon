package java8.jpoint.puzzlers;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Example2 {

    public static void main(String[] args) {
        ExecutorService ex = Executors.newSingleThreadExecutor();
        List<String> sentence = Arrays.asList("Казнить");

        ex.submit(() -> Files.write(Paths.get(""), sentence));          // Callable
        // ex.submit(() -> { Files.write(Paths.get(""), sentence); } ); // Runnable
    }
}
