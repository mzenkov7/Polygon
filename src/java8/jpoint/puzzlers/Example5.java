package java8.jpoint.puzzlers;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Example5 {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Вронский", "Поезд", "Анна");
        Comparator<String> cmp = Comparator.nullsLast(Comparator.naturalOrder());

        System.out.println(Collections.max(list, cmp));
        System.out.println(list.stream().collect(Collectors.maxBy(cmp)).get());
        System.out.println(list.stream().max(cmp).get());
        // Поезд
        // Поезд
        // Поезд

        List<String> list2 = Arrays.asList("Вронский", null, "Анна");
        Comparator<String> cmp2 = Comparator.nullsLast(Comparator.naturalOrder());

        System.out.println(Collections.max(list2, cmp2));
        System.out.println(list2.stream().collect(Collectors.maxBy(cmp2)).get());
        System.out.println(list2.stream().max(cmp2).get());
        // null
        // NoSuchElementException
        // NullPointerExceptio
    }
}
