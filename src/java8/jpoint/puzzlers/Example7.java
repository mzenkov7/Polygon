package java8.jpoint.puzzlers;

import java.util.function.Supplier;

public class Example7 {
    String str;

    public static void main(String[] args) {
        new Example7().run();
    }

    private void run() {
        str = "first";
        Supplier<String> s1 = str::toUpperCase;
        Supplier<String> s2 = () -> str.toUpperCase();

        str = "second";

        System.out.println(s1.get()); // FIRST
        System.out.println(s2.get()); // SECOND
    }
}
