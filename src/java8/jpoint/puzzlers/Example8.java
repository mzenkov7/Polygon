package java8.jpoint.puzzlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Example8 {

    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>(Arrays.asList("Arne", "Chuck", "Slay"));
        System.out.println(list1);
        // [Arne, Chuck, Slay]
        list1.removeIf("Chuck"::equals);
        System.out.println(list1);
        // [Arne, Slay]

        List<String> list2 = new ArrayList<>(Arrays.asList("Arne", "Chuck", "Slay"));
        list2.stream().forEach(x -> {
            if (x.equals("Chuck")) {
                list2.remove(x);
            }
        });
        // NullPointerException

        List<String> list3 = new ArrayList<>(Arrays.asList("Arne", "Slay", "Chuck"));
        list3.stream().forEach(x -> {
            if (x.equals("Chuck")) {
                list3.remove(x);
            }
        });
        // ConcurrentModificationException
    }
}
