package java8.lazy;

import java.util.function.Supplier;

public interface LazyString extends Supplier<String> {

    String get();

    default LazyString before(String str) {
        return () -> str + get();
    }

    default LazyString before(Supplier<String> supplier) {
        return () -> supplier.get() + get();
    }

    default LazyString after(String str) {
        return () -> get() + str;
    }

    default LazyString after(Supplier<String> supplier) {
        return () -> get() + supplier.get();
    }

    default LazyString around(String str) {
        return () -> str + get() + str;
    }

    default LazyString around(Supplier<String> supplier) {
        return () -> supplier.get() + get() + supplier.get();
    }

    static LazyString of(String origin) {
        return () -> origin;
    }

    static LazyString of(Supplier<String> origin) {
        return () -> origin.get();
    }
}
