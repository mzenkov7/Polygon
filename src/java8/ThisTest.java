package java8;

public class ThisTest {

    public static void main(String[] args) {
        ThisTest test = new ThisTest();
        test.go();
        test.go2();
    }

    private void go() {
        Lambda l = () -> System.out.println(this);
        l.action();     // java8.ThisTest
        l.thisPrint();  // java8.ThisTest$$Lambda$1
    }

    private void go2() {
        Lambda l = () -> { System.out.println(this); };
        l.action();     // java8.ThisTest
        l.thisPrint();  // java8.ThisTest$$Lambda$2
    }
}

interface Lambda {
    abstract void action();

    default void thisPrint() {
        System.out.println(this);
    }
}
