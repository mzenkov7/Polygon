package java8.dice;
import java.util.Random;
import java.util.stream.IntStream;

public class Dice {
    private final Random random = new Random();
    private final int faces;
    
    public Dice(int faces) {
        this.faces = faces;
    }

    public int singleThrow() {
        return random.nextInt(faces) + 1;
    }

    public int threeThrows() {
        return IntStream.generate(this::singleThrow)
                .limit(3)
                .sum();
    }

    // Without smallest throw
    public int specificThrowsWithShiftToGreat() {
        return IntStream.generate(this::singleThrow)
                .limit(4)
                .sorted()
                .skip(1)
                .sum();
    }

    // Without biggest throw
    public int specificThrowsWithShiftToLower() {
        return IntStream.generate(this::singleThrow)
                .limit(4)
                .sorted()
                .limit(3)
                .sum();
    }
}