package java8.dice;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class DiceThrowing {
    private static final int LIMIT = 200_000;

    public static void main(String[] args) {
        Dice dice = new Dice(6);
        List<IntSupplier> list = Arrays.asList(
                dice::singleThrow, 
                dice::threeThrows, 
                dice::specificThrowsWithShiftToGreat,
                dice::specificThrowsWithShiftToLower
                );
        list.forEach(DiceThrowing::printStat);
    }

    private static void printStat(IntSupplier supplier) {
        System.out.println(IntStream.generate(supplier::getAsInt)
                .limit(LIMIT)
                .summaryStatistics());
    }
}
