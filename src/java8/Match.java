package java8;

import java.util.Arrays;
import java.util.List;

public class Match {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("a1", "a2", "a3", "a1");

        System.out.println(list.stream().anyMatch("a1"::equals));
        // true

        System.out.println(list.stream().anyMatch("a8"::equals));
        // false
        System.out.println(list.stream().anyMatch(t -> "a8".equals(t)));
        // false, same as above

        System.out.println(list.stream().allMatch((s) -> s.contains("1")));
        // false
        System.out.println(list.stream().noneMatch("a7"::equals));
        // true
    }
}
