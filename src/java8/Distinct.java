package java8;

import java.util.Objects;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Distinct {

    public static void main(String[] args) {
        IntStream.generate(
                    () -> new Random().nextInt(10)
                )
                .limit(1000)
                .distinct()
                .forEach(i -> System.out.print(i + " "));
        System.out.println();
        Stream.of(
                    "qwe", 
                    "asd", 
                    "asd",
                    "zxc",
                    "qwe"
                )
                .distinct()
                .forEach(s -> System.out.print(s + " "));
        System.out.println();
        Stream.of(
                    new Holder(1, .3),
                    new Holder(1, .1 + .2), // (1, 0.30000000000000004)
                    new Holder(2, .2),
                    new Holder(1, .2), // <---
                    new Holder(1, .2), // same
                    new Holder(2, .1)
                )
                .distinct() // hashCode and equals both are necessary
                .forEach(System.out::println);
    }
}

class Holder {
    private int intVal;
    private double doubleVal;

    public Holder(int val, double d) {
        this.intVal = val;
        this.doubleVal = d;
    }

    @Override
    public String toString() {
        return "Holder [intVal=" + intVal + ", doubleVal=" + doubleVal + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(intVal, doubleVal);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Holder other = (Holder) obj;
        return Objects.equals(doubleVal, other.doubleVal)
               && Objects.equals(intVal, other.intVal);
    }
}