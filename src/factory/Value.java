package factory;

import java.util.concurrent.TimeUnit;

/**
 * Holder for video time position, given value and count of repeats.
 */
public class Value {
    private double time;
    private double value;
    private double repeats;

    Value(double time, double value, double repeats) {
        super();
        this.time = time;
        this.value = value;
        this.repeats = repeats;
    }

    public static void main(String[] args) {
        System.out.println(TimeUnit.SECONDS.toHours(Integer.MAX_VALUE));

        System.out.println(TimeUnit.HOURS.toSeconds(596));
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getRepeats() {
        return repeats;
    }

    public void setRepeats(double repeats) {
        this.repeats = repeats;
    }

    @Override
    public String toString() {
        return "Value [time=" + time + ", value=" + value + ", repeats=" + repeats + "]";
    }
}
