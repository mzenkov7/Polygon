package factory;

import java.util.function.Predicate;

/**
 * Factory for creation {@link Value} instance. <br>
 * Using implicit inheritance with lambdas.
 */
public class ValueFactoryLambda {
    private static int MAX_DURATION = 2145600; // 596 hours
    private final Predicate<Double> predicate;

    public static ValueFactoryLambda getAudioFactory() {
        return new ValueFactoryLambda(value -> (value < 0 || value > 100));
    }

    public static ValueFactoryLambda getPlaybackRateFactory() {
        return new ValueFactoryLambda(value -> (value <= 0 || value > 4));
    }

    public static ValueFactoryLambda getPositionFactory() {
        return new ValueFactoryLambda(value -> (value < 0 || value > MAX_DURATION));
    }

    private ValueFactoryLambda(Predicate<Double> predicate) {
        this.predicate = predicate;
    }

    private Value createValue(double time, double value, int repeats) {
        return new Value(time, value, repeats);
    }

    public Value create(double time, double value, int repeats) {
        checkArgs(time, repeats);
        checkValue(value);
        return createValue(time, value, repeats);
    }

    private void checkValue(double value) {
        if (predicate.test(value)) {
            throw new IllegalArgumentException();
        }
    }

    private void checkArgs(double time, int repeats) {
        if (time < 0 || repeats < 0) {
            throw new IllegalArgumentException();
        }
    }
}
