package factory;

/**
 * Factory for creation {@link Value} instance. <br>
 * Inheritance factory pattern.
 */
public abstract class ValueFactory {

    public static ValueFactory getAudioFactory() {
        return new AudioFactory();
    }

    public static ValueFactory getPlaybackRateFactory() {
        return new PlaybackRateFactory();
    }

    public static ValueFactory getPositionFactory() {
        return new PositionFactory();
    }

    protected Value createValue(double time, double value, int repeats) {
        return new Value(time, value, repeats);
    }

    public Value create(double time, double value, int repeats) {
        checkARGS(time, repeats);
        checkValue(value);
        return createValue(time, value, repeats);
    }

    protected abstract void checkValue(double value);

    private void checkARGS(double time, int repeats) {
        if (time < 0 || repeats < 0) {
            throw new IllegalArgumentException();
        }
    }
}

/**
 * Factory for creation Audio values. <br>
 * Allow value between 0 and 100.
 */
class AudioFactory extends ValueFactory {

    @Override
    protected void checkValue(double value) {
        if (value < 0 || value > 100) {
            throw new IllegalArgumentException();
        }
    }
}

/**
 * Factory for creation PlaybackRate values. <br>
 * Allow value between 0 and 4.
 */
class PlaybackRateFactory extends ValueFactory {

    @Override
    protected void checkValue(double value) {
        if (value <= 0 || value > 4) {
            throw new IllegalArgumentException();
        }
    }
}

/**
 * Factory for creation Play position values. <br>
 * Allow value between 0 and 2145600. <br>
 * (Where 2145600 = 596 hours)
 */
class PositionFactory extends ValueFactory {
    private static int MAX_DURATION = 2145600; // 596 hours

    @Override
    protected void checkValue(double value) {
        if (value < 0 || value > MAX_DURATION) {
            throw new IllegalArgumentException();
        }
    }
}