package net;

import java.io.DataOutputStream;

/**
 * Representation of client's connection.
 *
 */
public class User {

    private final int id;
    private final DataOutputStream dos;

    public User(int id, DataOutputStream dos) {
        this.id = id;
        this.dos = dos;
    }

    public int getId() {
        return id;
    }

    public DataOutputStream getDos() {
        return dos;
    }
}
