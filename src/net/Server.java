package net;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Console socket server. Take client connections. <br>
 * Give unique id and send random numbers.
 */
public class Server {
    private static final int SERVER_PORT = 6666;

    private final int port;
    AtomicInteger nextUserId = new AtomicInteger(0);

    public Server(int serverPort) {
        this.port = serverPort;
    }

    public static void main(String[] ar) {
        Server server = new Server(SERVER_PORT);
        server.start();
    }

    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.out.println("Server started");
            Worker worker = new Worker();
            Thread t = new Thread(worker);
            t.start();
            while (true) {
                Socket socket = serverSocket.accept();

                OutputStream sout = socket.getOutputStream();
                DataOutputStream out = new DataOutputStream(sout);

                int nextId = nextUserId.incrementAndGet();
                out.writeInt(nextId);

                worker.addUser(new User(nextId, out));
                System.out.println("Connected users : " + worker.getUserCount());
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }
}