package net;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

/**
 * Console socket client. Connect to server, get id. <br>
 * Take and print random number from server. <br>
 * Stop working on server down.
 */
public class Client {
    
    // Connect to localhost server instance. 
    private static final int serverPort = 6666;
    private static final String address = "127.0.0.1";
    private int id;

    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }

    public void start() {
        try (Socket socket = getSocket()) {
            System.out.println("Client started");
            InputStream sin = socket.getInputStream();

            DataInputStream in = new DataInputStream(sin);

            id = in.readInt();
            System.out.println("get id : " + id);

            int number = 0;
            while (true) {
                number = in.readInt();
                System.out.println("Current server number : " + number);
                System.out.println();
            }
        } catch (SocketException se) {
            System.out.println("Server down");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Socket getSocket() {
        try {
            InetAddress ipAddress = InetAddress.getByName(address);
            return new Socket(ipAddress, serverPort);
        } catch (Exception e) {
            return null;
        }
    }
}
