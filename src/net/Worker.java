package net;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Socket server worker. <br>
 * Send random numbers to connected users.
 */
public class Worker implements Runnable {

    private int number = 0;
    private final List<User> users = new ArrayList<>();
    Lock lock = new ReentrantLock();

    public void run() {
        while (true) {
            sleep();
            Iterator<User> it = users.iterator();
            while (it.hasNext()) {
                User user = it.next();
                try {
                    DataOutputStream stream = user.getDos();
                    stream.writeInt(number);
                    stream.flush();
                } catch (SocketException se) {
                    System.out.println("Client with id : (" + user.getId() + ") disconnected");
                    lock.lock();
                    it.remove();
                    lock.unlock();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            number = (int) (Math.random() * 100);
        }
    }

    private void sleep() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void addUser(User user) {
        lock.lock();
        users.add(user);
        lock.unlock();
    }
    
    public int getUserCount() {
        return users.size();
    }
}
