package list;

import java.util.Collection;
import java.util.List;
import java.util.function.UnaryOperator;

/**
 * Unmodifiable list implementation. All mutator methods throws
 * {@link UnsupportedOperationException}.
 *
 * @param <E>
 *            elements type
 */
abstract class UnmodifiableList<E> extends UnmodifiableCollection<E> implements List<E> {

    protected UnmodifiableList() {
    }

    @Override
    public final boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void replaceAll(UnaryOperator<E> operator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final E set(int index, E element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final void add(int index, E element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final E remove(int index) {
        throw new UnsupportedOperationException();
    }
}
