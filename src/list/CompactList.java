package list;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Unmodifiable list implementation that cover one element. <br>
 * Represent the element as list of identical elements. <br>
 * Take count of elements as parameter. <br>
 * If original element change, all elements in list change too.
 *
 * @param <E>
 *            elements type
 */
public final class CompactList<E> extends UnmodifiableList<E> implements List<E> {
    private final E element;
    private final int size;

    /**
     * Crate instance of CompactList.
     * 
     * @param element
     *            given element
     * @param size
     *            size of covering list
     */
    public CompactList(E element, int size) {
        checkArgs(element, size);
        this.element = element;
        this.size = size;
    }

    private void checkArgs(E element, int size) {
        Objects.requireNonNull(element);
        if (size < 1) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        Arrays.fill(array, element);
        return array;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object e : c) {
            if (!element.equals(e)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public E get(int index) {
        checkIndex(index);
        return element;
    }

    @Override
    public int indexOf(Object o) {
        return element.equals(o) ? 0 : -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return element.equals(o) ? size - 1 : -1;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        checkSunIndex(fromIndex, toIndex);
        return new CompactList<E>(element, toIndex - fromIndex);
    }

    @Override
    public Iterator<E> iterator() {
        return new CompactListIterator();
    }

    private class CompactListIterator implements Iterator<E> {
        protected int index;

        public CompactListIterator() {
            this.index = 0;
        }

        public CompactListIterator(int index) {
            this.index = index;
        }

        public boolean hasNext() {
            return index < size;
        }

        public E next() {
            if (!hasNext())
                throw new NoSuchElementException();
            index++;
            return element;
        }
    }

    @Override
    public ListIterator<E> listIterator() {
        return new ListItr();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return new ListItr(index);
    }

    private class ListItr extends CompactListIterator implements ListIterator<E> {

        private ListItr() {
        }

        private ListItr(int index) {
            super(index);
        }

        public boolean hasPrevious() {
            return index != 0;
        }

        public int nextIndex() {
            return index;
        }

        public int previousIndex() {
            return index - 1;
        }

        public E previous() {
            if (!hasPrevious())
                throw new NoSuchElementException();
            return element;
        }

        public void set(E e) {
            throw new UnsupportedOperationException();
        }

        public void add(E e) {
            throw new UnsupportedOperationException();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public String toString() {
        Iterator<E> it = iterator();

        StringBuilder sb = new StringBuilder();
        sb.append('[');
        while (it.hasNext()) {
            E e = it.next();
            sb.append(e);
            if (!it.hasNext()) {
                sb.append(']');
            } else {
                sb.append(',').append(' ');
            }
        }
        return sb.toString();
    }

    private void checkSunIndex(int from, int to) {
        checkIndex(from);
        checkIndex(to);
        if (from >= to) {
            throw new IllegalArgumentException("From index must be less than to");
        }
    }

    private void checkIndex(int index) {
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException();
        }
    }
}
