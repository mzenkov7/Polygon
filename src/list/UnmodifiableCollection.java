package list;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * Unmodifiable collection implementation. All mutator methods throws
 * {@link UnsupportedOperationException}.
 *
 * @param <E>
 *            elements type
 */
abstract class UnmodifiableCollection<E> implements Collection<E> {

    protected UnmodifiableCollection() {
    }

    @Override
    public final <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeIf(Predicate<? super E> filter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }
}
