package queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Master thread. Add one task every second. <br>
 * Wait if task queue is full.
 */
public class Master implements Runnable {

    private BlockingQueue<Work> tasks;

    public Master(BlockingQueue<Work> queue) {
        this.tasks = queue;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 30; i++) {
            try {

                TimeUnit.SECONDS.sleep(1);
                System.out.println("Add work with id = " + i);
                tasks.put(new Work(i));

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
