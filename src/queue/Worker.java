package queue;

import java.util.concurrent.BlockingQueue;

/**
 * Worker thread. Take and execute work from works queue.
 */
public class Worker implements Runnable {

    private BlockingQueue<Work> queue;

    public Worker(BlockingQueue<Work> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (!queue.isEmpty()) {
            try {

                Work w = queue.take();
                System.out.println("Worker took work with id = " + w.getId());
                w.action();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
