package queue;

import java.util.concurrent.TimeUnit;

/**
 * Work to be executed by worker. <br>
 * Execution take 4 seconds.
 */
public class Work {
    private final int id;

    public Work(int id) {
        super();
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void action() {
        sleep();
        System.out.println("Work with id = " + id + " compelted!");
    }

    private void sleep() {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
