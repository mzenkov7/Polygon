package concurrent.cache;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of {@link Cache}.<br>
 * Holds entities limited time. <br>
 * Remove policy : time to live.
 *
 * @param <T>
 *            type of storage objects
 */
public class TimeLimitedCache<T> implements Cache<T> {
    private final Object lock = new Object();

    private static final ThreadFactory THREAD_FACTORY = new DaemonThreadFactory();

    private static final Set<TimeUnit> notAllowed = EnumSet.range(TimeUnit.NANOSECONDS, TimeUnit.MICROSECONDS);

    private final long timeToLive;
    private final Map<String, Entry<T>> map;

    private TimeLimitedCache(TimeUnit timeToLiveUnit, long timeToLive, TimeUnit waitTimeUnit, long waitTime) {
        checkArgs(timeToLiveUnit, timeToLive, waitTimeUnit, waitTime);
        this.map = new HashMap<>();
        this.timeToLive = timeToLiveUnit.toMillis(timeToLive);
    }

    /**
     * Constructs cache with given parameters. <br>
     * <ul>
     * <li>Time to live of inner entries</li>
     * <li>Delay between launches of inner cleaner.</li>
     * </ul>
     * {@link TimeUnit } less than {@link TimeUnit#MILLISECONDS} not supported.
     * 
     * @param timeToLiveUnit
     *            time unit for entity expiration time
     * @param timeToLive
     *            time value of entity expiration time
     * @param waitTimeUnit
     *            time unit for cache clear mechanism
     * @param waitTime
     *            time value of cache clear mechanism
     * 
     * @param <T>
     *            type of storage objects
     * @return constructed object
     */
    public static <T> TimeLimitedCache<T> newInstance(TimeUnit timeToLiveUnit, long timeToLive, TimeUnit waitTimeUnit,
            long waitTime) {
        TimeLimitedCache<T> cache = new TimeLimitedCache<>(timeToLiveUnit, timeToLive, waitTimeUnit, waitTime);

        ScheduledExecutorService cleaner = Executors.newSingleThreadScheduledExecutor(THREAD_FACTORY);
        cleaner.scheduleWithFixedDelay(cache::removeExpired, 0, waitTime, waitTimeUnit);
        return cache;
    }

    private void checkArgs(TimeUnit timeToLiveUnit, long timeToLive, TimeUnit waitTimeUnit, long waitTime) {
        if (notAllowed.contains(timeToLiveUnit) || notAllowed.contains(waitTimeUnit)) {
            throw new IllegalArgumentException("Unsupported TimeUnits: " + notAllowed);
        }
        if (timeToLiveUnit.toMillis(timeToLive) <= 0 || waitTimeUnit.toMillis(waitTime) <= 0) {
            throw new IllegalArgumentException("time must be > 0");
        }
    }

    private TimeLimitedCache(long timeToLiveMilliseconds, long waitTimeMilliseconds) {
        this(TimeUnit.MILLISECONDS, timeToLiveMilliseconds, TimeUnit.MILLISECONDS, waitTimeMilliseconds);
    }

    /**
     * Constructs cache with given parameters. <br>
     * <ul>
     * <li>Time to live of inner entries</li>
     * <li>Delay between launches of inner cleaner.</li>
     * </ul>
     * {@link TimeUnit } less than {@link TimeUnit#MILLISECONDS} not supported.
     * 
     * @param timeToLiveMilliseconds
     *            milliseconds before expiration of entity time
     * @param waitTimeMilliseconds
     *            delay between running of cleaner mechanism
     * @param <T>
     *            type of storage objects
     * @return constructed object
     */
    public static <T> TimeLimitedCache<T> newInstance(long timeToLiveMilliseconds, long waitTimeMilliseconds) {
        return newInstance(TimeUnit.MILLISECONDS, timeToLiveMilliseconds, TimeUnit.MILLISECONDS, waitTimeMilliseconds);
    }

    @Override
    public void put(String key, T value) {
        long now = System.currentTimeMillis();
        final long entryExpirationTime = Long.MAX_VALUE - now > timeToLive ? now + timeToLive : Long.MAX_VALUE;

        synchronized (lock) {
            map.put(key, new Entry<T>(value, entryExpirationTime));
        }
    }

    @Override
    public T get(String key) {
        Entry<T> entry = map.get(key);
        if (entry == null || entry.isExpired()) {
            return null;
        }
        return entry.getValue();
    }

    private void removeExpired() {
        if (map.isEmpty()) {
            return;
        }

        Iterator<String> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (map.get(it.next()).isExpired()) {
                synchronized (lock) {
                    it.remove();
                }
            }
        }
    }

    @Override
    public int size() {
        Map<String, Entry<T>> snapshot = new HashMap<>(map);

        long size = 0;
        Iterator<String> it = snapshot.keySet().iterator();
        while (it.hasNext()) {
            Entry<T> entry = snapshot.get(it.next());
            if (!entry.isExpired()) {
                size++;
            }
        }
        return size > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) size;
    }

    @Override
    public String toString() {

        Map<String, Entry<T>> snapshot = new HashMap<>(map);
        if (snapshot.isEmpty()) {
            return "{}";
        }

        Iterator<String> it = snapshot.keySet().iterator();
        while (it.hasNext()) {
            Entry<T> entry = snapshot.get(it.next());
            if (entry.isExpired()) {
                it.remove();
            }
        }

        return snapshot.toString();
    }

    private class Entry<V> {
        private final V value;
        private final long expirationTime;

        public Entry(V value, long expirationTime) {
            this.value = value;
            this.expirationTime = expirationTime;
        }

        public V getValue() {
            return value;
        }

        public boolean isExpired() {
            return System.currentTimeMillis() > expirationTime;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }
}
