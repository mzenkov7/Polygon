package concurrent.cache;

/**
 * Interface for caching classes.
 */
public interface Cache<T> {

    /**
     * Put some object in cache by String key.
     * 
     * @param key
     *            Object key
     * @param value
     *            Saved value
     */
    public void put(String key, T value);

    /**
     * Get object from Cache by String key.
     * 
     * 
     * @param key
     *            object key
     * @return object by key, <br>
     *         {@code null} if not exist in cache
     */
    public T get(String key);

    /**
     * Return count of objects in cache.
     * 
     * @return count of objects
     */
    public int size();

}
