package concurrent.cache;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implementation of {@link Cache}.<br>
 * Holds limited count of objects. <br>
 * Remove policy : remove eldest.
 *
 * @param <T>
 *            type of storage objects
 */
public class SizeLimitedCache<T> implements Cache<T> {
    private final Map<String, T> map;
    private final Lock lock = new ReentrantLock();

    /**
     * Constructs cache with given parameter.
     *
     * @param maxSize
     *            max count of objects, that cache can hold
     */
    public SizeLimitedCache(final int maxSize) {
        this.map = new LimitedMap(maxSize);
    }

    private class LimitedMap extends LinkedHashMap<String, T> {
        private static final long serialVersionUID = 1L;
        private final int LIMIT;

        private LimitedMap(int maxSize) {
            super(maxSize, 1, true);
            LIMIT = maxSize;
        }

        protected boolean removeEldestEntry(Map.Entry<String, T> eldest) {
            return size() > LIMIT;
        }
    }

    @Override
    public void put(String key, T value) {
        lock.lock();
        try {
            map.put(key, value);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public T get(String key) {
        return map.get(key);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public String toString() {
        return map.toString();
    }
}
