package concurrent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Bug {

    private static final int SIZE = 200_000;

    // java.lang.ClassCastException: java.util.HashMap$Node cannot be cast to java.util.HashMap$TreeNode
    public static void main(String[] args) throws InterruptedException {
        stream();
        fork();
        executor();
    }

    private static void stream() {
        Map<String, String> map = new HashMap<>();
        IntStream.iterate(1, i -> i + 1).limit(SIZE).mapToObj(String::valueOf).parallel()
                .forEach((k) -> map.put(k, "String" + k));
        System.out.println(map.size());
    }

    private static void fork() throws InterruptedException {
        Map<String, String> map = new HashMap<>();

        ForkJoinPool pool = new ForkJoinPool();
        for (int i = 0; i < SIZE; i++) {
            int d = i;
            pool.execute(() -> map.put(String.valueOf(d), "String" + d));
        }
        pool.shutdown();
        while (!pool.isTerminated()) {
            pool.awaitTermination(10, TimeUnit.MILLISECONDS);
        }
        System.out.println(map.size());
    }

    private static void executor() {
        Map<String, String> map = new HashMap<>();

        ExecutorService service = Executors.newFixedThreadPool(4);
        for (int i = 0; i < SIZE; i++) {
            int d = i;
            service.execute(() -> map.put(String.valueOf(d), "String" + d));
        }
        service.shutdown();
        while (!service.isTerminated()) {
            try {
                service.awaitTermination(10, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(map.size());
    }
}
