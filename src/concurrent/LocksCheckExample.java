package concurrent;

/**
 * Check if current thread holds object monitor.
 */
public class LocksCheckExample {
    Object obj1 = new Object();
    Object obj2 = new Object();

    public static void main(String[] args) {
        new LocksCheckExample().go();
    }

    private void go() {
        printLocks();
        synchronized (obj1) {
            printLocks();
            synchronized (obj2) {
                printLocks();
            }
        }
    }

    private void printLocks() {
        System.out.println("Obj1 - " + Thread.holdsLock(obj1));
        System.out.println("Obj2 - " + Thread.holdsLock(obj2));
        System.out.println();
    }
}
