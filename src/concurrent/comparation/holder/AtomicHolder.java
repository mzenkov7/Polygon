package concurrent.comparation.holder;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicHolder implements Holder {
    private AtomicInteger atomic = new AtomicInteger(0);

    public void add(int add) {
        atomic.addAndGet(add);
    }

    public int get() {
        return atomic.get();
    }

    public void set(int i) {
        atomic.set(i);
    }
}
