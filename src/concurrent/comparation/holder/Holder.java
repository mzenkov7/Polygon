package concurrent.comparation.holder;

public interface Holder {

    void add(int diff);

    int get();

    void set(int i);
}
