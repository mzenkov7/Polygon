package concurrent.comparation.holder;

public class SimpleHolder implements Holder {
    private int simple = 0;

    public void add(int add) {
        simple += add;
    }

    public int get() {
        return simple;
    }

    @Override
    public void set(int i) {
        simple = i;
    }
}
