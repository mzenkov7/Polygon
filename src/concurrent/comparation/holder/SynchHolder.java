package concurrent.comparation.holder;

public class SynchHolder implements Holder {
    private volatile int synch = 0;

    public synchronized void add(int add) {
        synch += add;
    }

    public synchronized int get() {
        return synch;
    }

    public synchronized void set(int i) {
        synch = i;
    }
}
