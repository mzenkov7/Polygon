package concurrent.comparation;

import concurrent.comparation.holder.AtomicHolder;
import concurrent.comparation.holder.Holder;
import concurrent.comparation.holder.SimpleHolder;
import concurrent.comparation.holder.SynchHolder;

public class ComparationTest {

    public static void main(String[] args) {

        final int iterations = 100_000_000;

        Holder synch  = new SynchHolder();
        Holder atomic = new AtomicHolder();
        Holder simple = new SimpleHolder();

        Action synchAction  = new Action(synch, iterations );
        Action atomicAction = new Action(atomic, iterations);
        Action simpleAction = new Action(simple, iterations);

        System.out.println("Synch  : " + timed(synchAction ));
        System.out.println("Atomic : " + timed(atomicAction));
        System.out.println("Simple : " + timed(simpleAction));

        // Synch  : 3495.0
        // Atomic : 1062.0
        // Simple : 294.0

    }

    static double timed(Action action) {
        long start = System.currentTimeMillis();
        action.execute();
        long end = System.currentTimeMillis();
        return end - start;
    }
}

class Action {

    private Holder holder;
    private int iterations;

    public Action(Holder holder, int iterations) {
        this.holder = holder;
        this.iterations = iterations;
    }

    void execute() {
        for (int i = 0; i < iterations; i++) {
            holder.add(1);
        }
    }
}
