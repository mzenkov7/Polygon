package concurrent.comparation;

import java.util.Arrays;
import java.util.List;

import concurrent.comparation.holder.AtomicHolder;
import concurrent.comparation.holder.Holder;
import concurrent.comparation.holder.SimpleHolder;
import concurrent.comparation.holder.SynchHolder;

public class TestIncrement {

    public static void main(String[] args) throws InterruptedException {

        List<Holder> holders = Arrays.asList(new SimpleHolder(), new AtomicHolder(), new SynchHolder());

        for (int i = 0; i < 5; i++) {
            // TODO : use Executor
            Thread inc = new Thread(new Incrementator(holders, -1));
            Thread dec = new Thread(new Incrementator(holders, +1));

            inc.start();
            dec.start();

            inc.join();
            dec.join();

            for (Holder holder : holders) {
                System.out.println(holder.getClass().getSimpleName() + " : " + holder.get());
                holder.set(0);
            }

            System.out.println();
        }
    }
}

class Incrementator implements Runnable {
    int diff;
    List<Holder> holders;

    public Incrementator(List<Holder> holders, int diff) {
        this.diff = diff;
        this.holders = holders;
    }

    public void run() {
        for (int i = 0; i < 1_000_000; i++) {
            for (Holder holder : holders) {
                holder.add(diff);
            }
        }
    }
}
