package concurrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Using {link CountDownLatch} for synchronizing thread processing. <br>
 * After start, thread wait while other threads started. <br>
 * Main thread wait while all thread complete execution. 
 */
public class TestHarness {

    public static void main(String[] args) {

        Runnable r = () -> {
            try {
                System.out.println("Start");
                TimeUnit.MILLISECONDS.sleep(1000);
                System.out.println("End");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        TestHarness ts = new TestHarness();
        long time = ts.timeTasks(5, r);
        System.out.println(time);
        
        time = ts.timeTasks(10, r);
        System.out.println(time);
    }

    public long timeTasks(int nThreads, final Runnable task) {

        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(nThreads);

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread() {

                public void run() {
                    await(startGate);
                    try {
                        task.run();
                    } finally {
                        endGate.countDown();
                    }
                }
            };
            t.start();
        }
        long start = System.currentTimeMillis();
        startGate.countDown();
        await(endGate);
        long end = System.currentTimeMillis();
        return end - start;
    }

    private void await(final CountDownLatch startGate) {
        try {
            startGate.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}