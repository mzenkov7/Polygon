package concurrent.executor;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class RunningTaskExample {

    public static void main(String[] args) throws Exception {
        RunningTaskExample example = new RunningTaskExample();
        example.executeRunnable();
        // example.testSubmitRunnable();
        // example.testSubmitCallable();
    }

    public void executeRunnable() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            public void run() {
                sleep();
                print("Execute runnable done.");
            }
        });
        print("After execute runnable.");
        executor.shutdown();
    }

    public void submitRunnable() throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<?> future = executor.submit(new Runnable() {
            public void run() {
                sleep();
                print("Submit runnable done.");
            }
        });
        print("After submit runnable.");
        // Wait for execution of submitted runnable.
        future.get();
        print("After future get.");
        executor.shutdown();
    }

    public void submitCallable() throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(new Callable<String>() {
            public String call() throws Exception {
                sleep();
                print("Submit callable done.");
                return "Hello";
            }
        });
        print("After submit callable.");
        // Wait for execution of submitted callable and print its result.
        System.out.println(future.get());
        print("After future get.");
        executor.shutdown();
    }

    private void sleep() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void print(String message) {
        String threadName = Thread.currentThread().getName();
        Date now = new Date();
        System.out.println(now + " [" + threadName + "]: " + message);
    }
}