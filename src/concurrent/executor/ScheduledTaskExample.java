package concurrent.executor;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledTaskExample {

    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(new Task(), 0, 1, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(4);
        executor.shutdown();
    }
}

class Task implements Runnable {

    public void run() {
        try {
            TimeUnit.MILLISECONDS.sleep(500);
            System.out.println(new Date());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
