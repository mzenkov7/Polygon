package concurrent.executor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledPool {

    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);

        executor.scheduleAtFixedRate(new Command("Task1"), 0, 3, TimeUnit.SECONDS);
        executor.scheduleAtFixedRate(new Command("Task2"), 0, 3, TimeUnit.SECONDS);

        TimeUnit.SECONDS.sleep(10);
        executor.shutdown();
    }

}

class Command implements Runnable {
    private String s;

    Command(String s) {
        this.s = s;
    }

    public void run() {
        System.out.println(s + " start");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(s + " end");
    }
}
