package other;
public class InterfaceCast {

    public static void main(String[] args) {
        Square s = new Circle(5);
        System.out.println(s.square());
        Perimeter p = (Perimeter) s;
        System.out.println(p.perimeter());

        Square s2 = () -> 0;
        Perimeter p2 = (Perimeter) s2;

        p2.perimeter();
    }
}

interface Square {
    double square();
}

interface Perimeter {
    double perimeter();
}

class Circle implements Square, Perimeter {
    double r;

    public Circle(double r) {
        this.r = r;
    }

    public double perimeter() {
        return 2 * Math.PI * r;
    }

    public double square() {
        return Math.PI * r * r;
    }
}
