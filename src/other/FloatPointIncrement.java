package other;

public class FloatPointIncrement {
    static double i = 1 / 5.;

    public static void main(String args[]) {
        System.out.println(++i);
        System.out.println(++i);
        System.out.println(++i);
        System.out.println(++i);
        System.out.println(++i);
    }
}
