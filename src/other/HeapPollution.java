package other;
import java.util.ArrayList;
import java.util.List;

public class HeapPollution {

    public void go() {
        // List<Number>[] l1 = new ArrayList<Number>[10];
        List<?>[] l2 = new ArrayList<?>[10];
    }

    void process1() {
        List<? extends Number> list = new ArrayList<>();
        // list.add(234L);
        // list.add(100.);
        // list.add(13);
        // list.add(new Object());
        list.add(null);
    }

    public void go3() {
        List<? super Number> list = new ArrayList<>();
        list.add(null);
        list.add(new Integer(10));
        list.add(234L);
        list.add(100.);
        // list.add(new Object());
    }

    public static void main(String[] args) {
        String s = newList();
        System.out.println(s);
    }

    private static <T extends List<Integer>> T newList() {
        return (T) new ArrayList<Integer>();
    }
}
