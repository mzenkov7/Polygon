package other;
import java.math.BigDecimal;

public class BigDecimalUse {

    public static void main(String[] args) {
        double d = 1;
        BigDecimal bd = new BigDecimal(1);

        System.out.println(d);
        // 1.0

        d = d - 0.4;
        System.out.println(d);
        // 0.6

        d = d - 0.3;
        System.out.println(d);
        // 0.3

        d = d - 0.2;
        System.out.println(d);
        // 0.09999999999999998

        d = d - 0.1;
        System.out.println(d);
        // -2.7755575615628914E-17

        System.out.println(bd);
        // 1

        bd = bd.subtract(new BigDecimal("0.4"));
        System.out.println(bd);
        // 0.6

        bd = bd.subtract(new BigDecimal("0.3"));
        System.out.println(bd);
        // 0.3

        bd = bd.subtract(new BigDecimal("0.2"));
        System.out.println(bd);
        // 0.1

        bd = bd.subtract(new BigDecimal("0.1"));
        System.out.println(bd);
        // 0.0

        BigDecimal bd1 = new BigDecimal("0.4");
        BigDecimal bd2 = new BigDecimal(0.4);
        System.out.println(bd1.equals(bd2));
        // false

        BigDecimal bd3 = BigDecimal.valueOf(0.4);
        System.out.println(bd1.equals(bd3));
        // true
    }
}
