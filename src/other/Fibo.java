package other;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

import timer.TimeSummaryResult;
import timer.Timer;

/**
 * Comparison of fibo execution in cycle and recursion
 */
public class Fibo {

    public static void main(String[] args) {
        Fibo fibo = new Fibo();

        fibo.go(10); // Ratio: recursion / cycle = ~ 2-3
        fibo.go(30); // Ratio: recursion / cycle = ~ 10_252
        // ~ 4 min
        // fibo.go(50); // Ratio: recursion / cycle = ~ 4_258_071
    }

    private void go(final int N) {
        Timer timer = Timer.newInstance(TimeUnit.NANOSECONDS);
        timer.warmUp(() -> { cycle(10); recursion(10); }, 1_000);

        System.out.println("N = " + N);

        TimeSummaryResult<BigInteger> recursion = timer.time(() -> recursion(N));
        System.out.println(recursion.getResult());
        System.out.println(recursion.getTime() + " ns");

        TimeSummaryResult<BigInteger> cycle = timer.time(() -> cycle(N));
        System.out.println(cycle.getResult());
        System.out.println(cycle.getTime() + " ns");

        System.out.println("recursion / cycle = " + (recursion.getTime() / cycle.getTime()));
        System.out.println();
    }

    private BigInteger recursion(int N) {
        if (N < 3) {
            return BigInteger.ONE;
        }
        return recursion(N - 1).add(recursion(N - 2));
    }

    private BigInteger cycle(int N) {
        BigInteger first = BigInteger.ZERO;
        BigInteger second = BigInteger.ONE;
        BigInteger result = BigInteger.ZERO;
        for (int n = 1; n < N; n++) {
            BigInteger current = first.add(second);
            first = second;
            second = current;
            result = current;
        }
        return result;
    }
}
