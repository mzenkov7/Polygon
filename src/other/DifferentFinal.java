package other;

public class DifferentFinal {
    public static void main(String[] args) {

        final int a = 0;    // Compile time

        final int b;        // Runtime
        b = 10;             // Runtime

        int x = 0;
        switch (x) {
        case a:
            System.out.println("a");
            break;

        // Allowed only oneline initialize final variable.
        // Error : case expressions must be constant expressions
//      case b:
//      System.out.println("b");
//      break;

        default:
            System.out.println("default");
            break;
        }
    }
}
