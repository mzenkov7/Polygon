package other;

public class ParamStatement {

    public static void main(String[] args) {
        int x = 5;
        printParam(x, x = x + 5);
        // 5 : 10

        x = 5;
        printParam(x = x + 5, x);
        // 10 : 10
    }

    public static void printParam(int a, int b) {
        System.out.println(a + " : " + b);
    }
}