package other;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class LRUSet {

    public static void main(String[] args) {
        Map<String, Boolean> map = new LRUMap();

        // Set contains backed copy of map.
        Set<String> LRUset = Collections.newSetFromMap(map);

        LRUset.add("2");
        LRUset.add("1");
        LRUset.add("7");
        LRUset.add("12");
        LRUset.add("4");
        LRUset.add("4");
        LRUset.add("12");
        LRUset.add("3");

        System.out.println(LRUset);
    }
}

class LRUMap extends LinkedHashMap<String, Boolean> {
    private static final long serialVersionUID = 1L;
    private static final int LIMIT = 5;

    public LRUMap() {
        super(16, 0.75f, true);
    }

    @Override
    protected boolean removeEldestEntry(Entry<String, Boolean> eldest) {
        return size() > LIMIT;
    }
}
