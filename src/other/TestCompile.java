package other;
public class TestCompile {
    public static void main() {
        C1 cl1 = new C1();
        C2 cl2 = new C2();
        Integer in = new Integer(4);

        // I2 y = (I2) in; // compiler complains here !!
        // I2 x = (I2) cl1; // compiler complains too
    }
}

interface I1 {
}

final class C1 implements I1 {
}

interface I2 {
}

class C2 implements I2 {
}