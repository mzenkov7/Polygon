package other;
public class AmbiguousLambda {

    public static void main(String[] args) {

        // The method test(Test1) is ambiguous for the type AmbiguousLambda
        // test(() -> System.out.println("Data"));

    }

    private static void test(Test1 t) {
        t.action();
    }

    private static void test(Test2 t) {
        t.action();
    }
}

interface Test1 {
    void action();
}

interface Test2 {
    void action();
}
