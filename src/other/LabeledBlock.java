package other;

import java.util.Random;

public class LabeledBlock {

    public static void main(String[] args) {
        boolean bool = new Random().nextBoolean();
        System.out.println("Start of main");
        label: {
            System.out.println("In block");
            if (bool) {
                break label;
            }
            System.out.println("End of block"); // Doesn't show if bool = true
        }
        System.out.println("End of main");
    }
}
