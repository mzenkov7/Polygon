package other;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class EnumSets {

    public static void main(String[] args) {
        Set<TimeUnit> setOf = EnumSet.of(TimeUnit.NANOSECONDS, TimeUnit.SECONDS);
        Set<TimeUnit> setRange = EnumSet.range(TimeUnit.NANOSECONDS, TimeUnit.SECONDS);

        System.out.println(setOf);      // [NANOSECONDS, SECONDS]
        System.out.println(setRange);   // [NANOSECONDS, MICROSECONDS, MILLISECONDS, SECONDS]

        Set<TimeUnit> set = EnumSet.range(TimeUnit.NANOSECONDS, TimeUnit.MICROSECONDS);

        System.out.println(set.contains(TimeUnit.NANOSECONDS)); // true
        System.out.println(set.contains(TimeUnit.MINUTES));     // false
    }
}
