package other;

import java.util.Arrays;

public class VarArgs {

    public static void main(String[] args) {

        varArgMethod(new Object[] { 1, 2, 3 });
        // 3

        varArgMethod(new Object[] { 1, 2, 3 }, 2);
        // 2

        varArgMethod(new Object[] { 1, 2, 3 }, new Object[] { 4, 5, 6 });
        // 2

        varArgMethod(Arrays.asList(1, 2, 3));
        // 1
    }

    private static void varArgMethod(Object... obj) {
        System.out.println(obj.length);
    }
}
