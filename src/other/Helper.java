package other;
import java.util.Arrays;
import java.util.List;

// if class is generic, all generic methods will crash

public class Helper// <T>
{

    public List<Integer> numbers() {
        return Arrays.asList(10, 20);
    }

    public static void main(String[] args) {

        Helper helper = new Helper();
        for (Integer number : helper.numbers()) {
            System.out.println(number);
        }
    }
}
