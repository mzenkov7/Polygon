package other;

@SuppressWarnings("finally")
public class Finally {

    public static void main(String[] args) {

        System.out.println(first());
        // 1

        System.out.println(second());
        // 3

        System.out.println(third());
        // 3
    }

    public static int first() {
        int x = 0;
        try {
            x = 1;
            return x;
        } catch (Exception E) {
            x = 2;
            return x;
        } finally {
            x = 3;
        }
    }

    public static int second() {
        int x = 0;
        try {
            x = 1;
            return x;
        } catch (Exception E) {
            x = 2;
            return x;
        } finally {
            x = 3;
            return x;
        }
    }

    public static int third() {
        int x = 0;
        try {
            throw new RuntimeException();
        } finally {
            x = 3;
            return x;
        }
    }
}
