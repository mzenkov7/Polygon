package timer.reflection;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import timer.Action;

/**
 * Implementation of {@link Timer} interface. <br>
 * Use {@link TimerHandler} for measuring duration of method execution.
 * 
 * @see timer.test.TimerImpl
 */
@Deprecated
class TimerImpl implements Timer {
    TimeUnit timeUnit;
    
    public TimerImpl(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    @Override
    public void time(Action action) {
        action.execute();
    }

    @Override
    public <T> T time(Supplier<T> supplier) {
        return supplier.get();
    }
}
