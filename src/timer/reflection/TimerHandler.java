package timer.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import timer.simple.Timer;

/**
 * Proxy handler for instance of {@link Timer}. <br>
 * Measured duration of all public method execution.
 */
@Deprecated
class TimerHandler implements InvocationHandler {
    private TimerImpl timer;

    /**
     * Construct proxy object for given {@code TimerImpl} instance.
     */
    public TimerHandler(TimerImpl obj) {
        this.timer = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        if (Modifier.isPublic(method.getModifiers())) {
            long start = System.nanoTime();
            result = method.invoke(timer, args);
            long end = System.nanoTime();
            System.out.println(timer.timeUnit.convert((end - start), TimeUnit.NANOSECONDS));
        }
        return result;
    }
}
