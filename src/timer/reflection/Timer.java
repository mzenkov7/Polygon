package timer.reflection;

import java.lang.reflect.Proxy;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import timer.Action;

/**
 * Class for measuring the duration of the actions. <br>
 * Support both methods with and without return value. <br>
 * Use proxy class for measuring. <br>
 * Print measured time in standard output stream. <br>
 * Time information convert by given {@link TimeUnit} instance.
 *
 * @see timer.Timer
 */
@Deprecated
public interface Timer {

    /**
     * Timer instance factory method. Create timer instance based on given
     * {@code TimeUnit}.
     * 
     * @return Timer instance based on {@code TimeUnit}
     */
    public static Timer newInstance(TimeUnit unit) {
        TimerImpl timer = new TimerImpl(unit);
        return (Timer) Proxy.newProxyInstance(Timer.class.getClassLoader(), new Class<?>[] { Timer.class },
                new TimerHandler(timer));
    }

    /**
     * Default timer instance factory method.
     * 
     * @return Timer instance based on default {@code TimeUnit}
     */
    public static Timer newInstance() {
        return newInstance(TimeUnit.MILLISECONDS);
    }

    /**
     * Take action without result and measure its execution duration.
     * 
     * @param action
     *            given action
     */
    public void time(Action action);

    /**
     * Take action with result and measure its execution duration. <br>
     * Return result of action executing.
     * 
     * @param supplier
     *            given action
     * @param <T>
     *            type of resulting value
     * @return result of action executing
     */
    public <T> T time(Supplier<T> supplier);
}
