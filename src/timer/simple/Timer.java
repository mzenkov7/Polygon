package timer.simple;

import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;
import java.util.function.Supplier;

import timer.Action;

/**
 * Class for measuring the duration of the actions. <br>
 * Support both methods with and without return value. <br>
 * Provide methods for primitive types. <br>
 * Print measured time in standard output stream. <br>
 * Time information convert by given {@link TimeUnit} instance.
 *
 * @see timer.Timer
 */
@Deprecated
public interface Timer {

    /**
     * Default timer instance factory method.
     * 
     * @return Timer instance based on default {@code TimeUnit}
     */
    public static Timer newInstance(TimeUnit unit) {
        return new TimerImpl(unit);
    }

    /**
     * Timer instance factory method. Create timer instance based on given
     * {@code TimeUnit}.
     * 
     * @return Timer instance based on {@code TimeUnit}
     */
    public static Timer newInstance() {
        return newInstance(TimeUnit.MILLISECONDS);
    }

    /**
     * Take action without result and measure its execution duration.
     * 
     * @param action
     *            given action
     */
    public void time(Action action);

    /**
     * Take action with result and measure its execution duration. <br>
     * Return result of action executing.
     * 
     * @param supplier
     *            given action
     * @param <T>
     *            type of resulting value
     * @return result of action executing
     */
    public <T> T time(Supplier<T> supplier);

    /**
     * Take action with {@code int} result and measure its execution duration.
     * <br>
     * Return result of action executing.
     * 
     * @param supplier
     *            given action
     * @return result of action executing
     */
    public int time(IntSupplier supplier);

    /**
     * Take action with {@code long} result and measure its execution duration. <br>
     * Return result of action executing.
     * 
     * @param supplier
     *            given action
     * @return result of action executing
     */
    public long time(LongSupplier supplier);

    /**
     * Take action with {@code double} result and measure its execution duration. <br>
     * Return result of action executing.
     * 
     * @param supplier
     *            given action
     * @return result of action executing
     */
    public double time(DoubleSupplier supplier);

    /**
     * Take action with {@code boolean} result and measure its execution duration. <br>
     * Return result of action executing.
     * 
     * @param supplier
     *            given action
     * @return result of action executing
     */
    public boolean time(BooleanSupplier supplier);
}