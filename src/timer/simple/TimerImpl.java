package timer.simple;

import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;
import java.util.function.Supplier;

import timer.Action;

/**
 * Implementation of {@link Timer} interface. <br>
 * Provide method for working with primitive types.
 * 
 * @see timer.test.TimerImpl
 */
@Deprecated
class TimerImpl implements Timer {
    private TimeUnit timeUnit;

    /**
     * Create instance of {@link Timer} based on given {@code TimeUnit}
     * 
     * @param timeUnit
     *            time measurement base
     */
    public TimerImpl(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    @Override
    public void time(Action action) {
        long start = System.nanoTime();
        action.execute();
        long end = System.nanoTime();
        System.out.println(timeUnit.convert((end - start), TimeUnit.NANOSECONDS));
    }

    @Override
    public <T> T time(Supplier<T> supplier) {
        long start = System.nanoTime();
        T result = supplier.get();
        long end = System.nanoTime();
        System.out.println(timeUnit.convert((end - start), TimeUnit.NANOSECONDS));
        return result;
    }

    @Override
    public int time(IntSupplier supplier) {
        long start = System.nanoTime();
        int result = supplier.getAsInt();
        long end = System.nanoTime();
        System.out.println(timeUnit.convert((end - start), TimeUnit.NANOSECONDS));
        return result;
    }

    @Override
    public long time(LongSupplier supplier) {
        long start = System.nanoTime();
        long result = supplier.getAsLong();
        long end = System.nanoTime();
        System.out.println(timeUnit.convert((end - start), TimeUnit.NANOSECONDS));
        return result;
    }

    @Override
    public double time(DoubleSupplier supplier) {
        long start = System.nanoTime();
        double result = supplier.getAsDouble();
        long end = System.nanoTime();
        System.out.println(timeUnit.convert((end - start), TimeUnit.NANOSECONDS));
        return result;
    }

    @Override
    public boolean time(BooleanSupplier supplier) {
        long start = System.nanoTime();
        boolean result = supplier.getAsBoolean();
        long end = System.nanoTime();
        System.out.println(timeUnit.convert((end - start), TimeUnit.NANOSECONDS));
        return result;
    }
}
