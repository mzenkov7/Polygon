package timer;

/**
 * Representation of time measurement. Contains duration of action and result
 * value. <br>
 * Can convert time by given {@code TimeUnit}.
 * 
 * @param <T>
 *            value type
 * 
 * @see Timer#time(java.util.function.Supplier)
 */
public interface TimeSummaryResult<T> extends TimeSummary {

    /**
     * Return result of action executing.
     * 
     * @return action result value
     */
    T getResult();
}