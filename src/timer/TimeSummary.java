package timer;

import java.util.concurrent.TimeUnit;

/**
 * Representation of time measurement. Contains duration of action. <br>
 * Can convert time by given {@code TimeUnit}.
 * 
 * @see Timer#time(Action)
 */
public interface TimeSummary {

    /**
     * Return duration of executed action as {@code long} value. By default,
     * using TimeUnit from {@link Timer} instance.
     * 
     * @return duration of action executing
     */
    long getTime();

    /**
     * Return duration of executed action as {@code long} value. Convert time
     * from base {@code TimeUnit} to given.
     * 
     * @param timeUnit
     *            given TimeUnit to convert time
     * @return duration of action executing
     */
    long getTime(TimeUnit timeUnit);
}