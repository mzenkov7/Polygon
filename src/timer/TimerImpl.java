package timer;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Implementation of {@link Timer} interface.
 */
final class TimerImpl implements Timer {
    private TimeUnit timeUnit;

    /**
     * Create instance of {@link Timer} based on given {@code TimeUnit}.
     * 
     * @param timeUnit
     *            time measurement base
     */
    public TimerImpl(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    @Override
    public TimeSummary time(Action action) {
        long start = System.nanoTime();
        action.execute();
        long end = System.nanoTime();
        long duration = end - start;
        TimeSummary summary = new TimeSummaryImpl(timeUnit, duration);
        return summary;
    }

    @Override
    public <T> TimeSummaryResult<T> time(Supplier<T> supplier) {
        long start = System.nanoTime();
        T result = supplier.get();
        long end = System.nanoTime();
        long duration = end - start;
        TimeSummaryResult<T> summary = new TimeSummaryResultImpl<>(timeUnit, duration, result);
        return summary;
    }
}
