package timer;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Class for measuring the duration of the executable actions. <br>
 * Support both methods with and without return value. <br>
 * 
 * Returning time information based on given {@code TimeUnit} instance.
 */
public interface Timer {

    /**
     * Timer instance factory method. Create timer instance based on given
     * {@code TimeUnit}.
     * 
     * @param unit
     *            base {@code TimeUnit} for measuring
     * 
     * @return Timer instance based on given {@code TimeUnit}
     */
    static Timer newInstance(TimeUnit unit) {
        return new TimerImpl(unit);
    }

    /**
     * Default timer instance factory method with {@code TimeUnit.MILLISECONDS}
     * as base.
     * 
     * @return Timer instance based on default {@code TimeUnit}
     */
    static Timer newInstance() {
        return newInstance(TimeUnit.MILLISECONDS);
    }

    /**
     * Preparation actions before time measuring. Count of iteration have to be
     * a positive integer.
     * 
     * @param action
     *            action to be prepared.
     * @param iterations
     *            of invocation preparation actions.
     */
    default void warmUp(Action action, int iterations) {
        Objects.requireNonNull(action);
        if (iterations <= 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < iterations; i++) {
            action.execute();
        }
    }

    /**
     * Take action without result and measure its execution duration. <br>
     * Return instance of {@link TimeSummary} with information about duration of
     * action.
     * 
     * @param action
     *            given action
     * @return time measurement information
     */
    TimeSummary time(Action action);

    /**
     * Take action with result and measure its execution duration. <br>
     * Return instance of {@link TimeSummaryResult} with information about
     * duration of action and result of action.
     * 
     * @param supplier
     *            given action
     * @param <T>
     *            type of resulting value
     * @return time measurement information with resulting value
     */
    <T> TimeSummaryResult<T> time(Supplier<T> supplier);
}
