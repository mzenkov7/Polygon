package timer;

/**
 * Representation of executable task without returning any result.
 */
@FunctionalInterface
public interface Action {
    
    /**
     * Take any executable action.
     */
    void execute();
}