package timer;

import java.util.concurrent.TimeUnit;

/**
 * Implementation of {@link TimeSummaryResult}.
 */
class TimeSummaryResultImpl<T> extends TimeSummaryImpl implements TimeSummaryResult<T> {
    private final T result;

    /**
     * Create {@code TimeSummaryResult} instance with given parameters.
     * 
     * @param timeUnit
     *            time representation unit
     * @param time
     *            duration of method executing based on given TimeUnit
     * @param result
     *            result of method executing
     */
    TimeSummaryResultImpl(TimeUnit timeUnit, long time, T result) {
        super(timeUnit, time);
        this.result = result;
    }

    @Override
    public T getResult() {
        return result;
    }
}
