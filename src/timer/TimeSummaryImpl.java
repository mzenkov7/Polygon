package timer;

import java.util.concurrent.TimeUnit;

/**
 * Implementation of {@link TimeSummary}.
 */
class TimeSummaryImpl implements TimeSummary {
    protected final TimeUnit timeUnit;
    protected final long time;

    /**
     * Create {@code TimeSummary} instance with given parameters.
     * 
     * @param timeUnit
     *            time representation unit
     * @param time
     *            duration of method executing based on given TimeUnit
     */
    TimeSummaryImpl(TimeUnit timeUnit, long time) {
        this.timeUnit = timeUnit;
        this.time = time;
    }

    @Override
    public long getTime() {
        return timeUnit.convert((time), TimeUnit.NANOSECONDS);
    }

    @Override
    public long getTime(TimeUnit timeUnit) {
        return timeUnit.convert((time), TimeUnit.NANOSECONDS);
    }
}
