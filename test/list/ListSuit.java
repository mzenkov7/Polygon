package list;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CompactListTest.class, UnmodifibleListTest.class, UnmodifibleCollectionTest.class })
public class ListSuit {
}
