package list;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CompactListTest {

    private interface Factory {
        List<String> create(String str, Integer size);
    }

    private static final String NEW_STRING = "NEW";
    private static final String TEST_STRING = "Test";
    private static final int TEST_SIZE = 20;

    private static final Factory factory = CompactList<String>::new;
    private static final List<String> TEST_LIST = factory.create(TEST_STRING, TEST_SIZE);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testEmptyObject() {
        thrown.expect(NullPointerException.class);
        factory.create(null, TEST_SIZE);
    }

    @Test
    public void testZeroSize() {
        thrown.expect(IllegalArgumentException.class);
        factory.create(TEST_STRING, 0);
    }

    @Test
    public void testNegativeSize() {
        thrown.expect(IllegalArgumentException.class);
        factory.create(TEST_STRING, -1000);
    }

    @Test
    public void testPositiveSize() {
        List<String> list = TEST_LIST;
        assertEquals(TEST_SIZE, list.size());
    }

    @Test
    public void testIsEmpty() {
        List<String> list = TEST_LIST;
        assertFalse(list.isEmpty());
    }

    @Test
    public void testContains() {
        List<String> list = TEST_LIST;
        assertTrue(list.contains(TEST_STRING));
    }

    @Test
    public void testNotContains() {
        List<String> list = TEST_LIST;
        assertFalse(list.contains(NEW_STRING));
    }

    @Test
    public void testToArray() {
        List<String> list = TEST_LIST;
        assertTrue(Arrays.equals(getArray(TEST_STRING, TEST_SIZE), list.toArray()));
    }

    @Test
    public void testContainsAll() {
        List<String> list = TEST_LIST;
        assertTrue(list.containsAll(getArrayList(TEST_STRING, TEST_SIZE)));
    }

    @Test
    public void testContainsDiffObjAll() {
        List<String> list = TEST_LIST;
        assertFalse(list.containsAll(getDiffObjArrayList(TEST_STRING, TEST_SIZE)));
    }

    @Test
    public void testNotContainsAll() {
        List<String> list = TEST_LIST;
        assertFalse(list.containsAll(getArrayList(NEW_STRING, TEST_SIZE)));
    }

    @Test
    public void testGetInArraySize() {
        List<String> list = TEST_LIST;
        assertEquals(TEST_STRING, list.get(TEST_SIZE - 1));
    }

    @Test
    public void testGetOutArraySize() {
        List<String> list = TEST_LIST;
        thrown.expect(IndexOutOfBoundsException.class);
        list.get(TEST_SIZE + 1);
    }

    @Test
    public void testGetNegativeIndex() {
        List<String> list = TEST_LIST;
        thrown.expect(IndexOutOfBoundsException.class);
        list.get(-1);
    }

    @Test
    public void testIndexOfContainsObject() {
        List<String> list = TEST_LIST;
        assertEquals(0, list.indexOf(TEST_STRING));
    }

    @Test
    public void testIndexOfNotContainsObject() {
        List<String> list = TEST_LIST;
        assertEquals(-1, list.indexOf(NEW_STRING));
    }

    @Test
    public void testLastIndexOfContainsObject() {
        List<String> list = TEST_LIST;
        assertEquals(list.size() - 1, list.lastIndexOf(TEST_STRING));
    }

    @Test
    public void testLastIndexOfNotContainsObject() {
        List<String> list = TEST_LIST;
        assertEquals(-1, list.lastIndexOf(NEW_STRING));
    }

    @Test
    public void testSubList() {
        List<String> list = TEST_LIST;
        final int FROM_INDEX = 5;
        final int TO_INDEX = 15;
        assertEquals(getArrayList(TEST_STRING, 10), list.subList(FROM_INDEX, TO_INDEX));
    }

    @Test
    public void testSubListReplaceBoundIndexes() {
        List<String> list = TEST_LIST;
        final int FROM_INDEX = 5;
        final int TO_INDEX = 15;
        thrown.expect(IllegalArgumentException.class);
        assertEquals(getArrayList(TEST_STRING, 10), list.subList(TO_INDEX, FROM_INDEX));
    }

    @Test
    public void testIterator() {
        List<String> list = TEST_LIST;
        Iterator<String> it = list.iterator();
        int size = 0;
        while (it.hasNext()) {
            size++;
            it.next();
        }
        assertEquals(TEST_SIZE, size);
    }

    @Test
    public void testIteratorOverSize() {
        List<String> list = TEST_LIST;
        Iterator<String> it = list.iterator();
        thrown.expect(NoSuchElementException.class);
        for (int i = 0; i < list.size() + 1; i++) {
            it.next();
        }
    }

    @Test
    public void testToString() {
        List<String> list = TEST_LIST;
        final String EXPECTED = getArrayList(TEST_STRING, TEST_SIZE).toString();
        final String ACTUAL = list.toString();
        assertEquals(EXPECTED, ACTUAL);
    }

    private String[] getArray(String element, int size) {
        String[] arr = new String[size];
        for (int i = 0; i < size; i++) {
            arr[i] = element;
        }
        return arr;
    }

    private List<String> getArrayList(String element, int size) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(element);
        }
        return list;
    }

    private List<String> getDiffObjArrayList(String element, int size) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add((new Date()).toString());
        }
        list.set(0, element);
        return list;
    }
}
