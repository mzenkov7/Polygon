package list;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class UnmodifibleListTest {

    private static final String NEW_STRING = "NEW";
    private static final String TEST_STRING = "Test";
    private static final int TEST_SIZE = 20;

    private static final UnmodifiableList<String> TEST_LIST = new CompactList<>(TEST_STRING, TEST_SIZE);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testAddAllAtIndex() {
        List<String> list = TEST_LIST;
        thrown.expect(UnsupportedOperationException.class);
        list.addAll(0, getArrayList(NEW_STRING, TEST_SIZE));
    }

    @Test
    public void testReplaceAll() {
        List<String> list = TEST_LIST;
        thrown.expect(UnsupportedOperationException.class);
        list.replaceAll(s -> s + "suf");
    }
    
    @Test
    public void testSetAtIndex() {
        List<String> list = TEST_LIST;
        thrown.expect(UnsupportedOperationException.class);
        list.set(0, NEW_STRING);
    }

    @Test
    public void testAddAtIndex() {
        List<String> list = TEST_LIST;
        thrown.expect(UnsupportedOperationException.class);
        list.add(0, NEW_STRING);
    }

    @Test
    public void testRemoveAtIndex() {
        List<String> list = TEST_LIST;
        thrown.expect(UnsupportedOperationException.class);
        list.remove(0);
    }

    private List<String> getArrayList(String element, int size) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(element);
        }
        return list;
    }
}
