package list;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class UnmodifibleCollectionTest {

    private static final String NEW_STRING = "NEW";
    private static final String TEST_STRING = "Test";
    private static final int TEST_SIZE = 20;

    private static final UnmodifiableCollection<String> TEST_LIST = new CompactList<>(TEST_STRING, TEST_SIZE);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGenericToArray() {
        thrown.expect(UnsupportedOperationException.class);
        TEST_LIST.toArray(new Integer[0]);
    }

    @Test
    public void testAddElement() {
        thrown.expect(UnsupportedOperationException.class);
        TEST_LIST.add(NEW_STRING);
    }

    @Test
    public void testAddAll() {
        thrown.expect(UnsupportedOperationException.class);
        TEST_LIST.addAll(getArrayList(NEW_STRING, TEST_SIZE));
    }

    @Test
    public void testRemoveElement() {
        thrown.expect(UnsupportedOperationException.class);
        TEST_LIST.remove(NEW_STRING);
    }

    @Test
    public void testRemoveAll() {
        thrown.expect(UnsupportedOperationException.class);
        TEST_LIST.removeAll(getArrayList(NEW_STRING, TEST_SIZE));
    }
    
    @Test
    public void testRemoveIf() {
        thrown.expect(UnsupportedOperationException.class);
        TEST_LIST.removeIf(TEST_STRING::equals);
    }

    @Test
    public void testRetainAll() {
        thrown.expect(UnsupportedOperationException.class);
        TEST_LIST.retainAll(getArrayList(NEW_STRING, TEST_SIZE));
    }

    @Test
    public void testClear() {
        thrown.expect(UnsupportedOperationException.class);
        TEST_LIST.clear();
    }

    private Collection<String> getArrayList(String element, int size) {
        Collection<String> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(element);
        }
        return list;
    }
}
