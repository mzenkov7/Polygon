package timer;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TimerTest {
    int counter = 0;
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testTimeMethodDefaultTimeUnit() {
        Timer timer = Timer.newInstance();
        TimeSummary ts = timer.time(() -> sleep(10));
        long time = ts.getTime();

        assertTrue(9 <= time && time <= 11);
    }

    @Test
    public void testTimeMethodWithGivenTimeUnit() {
        Timer timer = Timer.newInstance(TimeUnit.MICROSECONDS);
        TimeSummary ts = timer.time(() -> sleep(10));
        long time = ts.getTime();

        assertTrue(9_000 <= time && time <= 11_000);
    }

    @Test
    public void testTimeMethodDefaultTimeUnitConvert() {
        Timer timer = Timer.newInstance();
        TimeSummary ts = timer.time(() -> sleep(10));
        long time = ts.getTime(TimeUnit.MICROSECONDS);

        assertTrue(9_000 <= time && time <= 11_000);
    }

    @Test
    public void testTimeMethodWithResult() {
        Timer timer = Timer.newInstance();
        TimeSummaryResult<String> tsr = timer.time(() -> sleepReturn(15));
        long time = tsr.getTime(TimeUnit.MICROSECONDS);
        String result = tsr.getResult();

        assertTrue(14_000 <= time && time <= 17_000);
        assertEquals("result", result);
    }

    @Test
    public void testTimeMethodWithWarmUp() {
        Timer timer = Timer.newInstance();

        counter = 0;
        timer.warmUp(() -> counter++, 100);

        TimeSummary ts = timer.time(() -> sleep(15));
        long time = ts.getTime(TimeUnit.MICROSECONDS);

        assertTrue(14_000 <= time && time <= 17_000);
        assertEquals(100, counter);
    }
    
    @Test
    public void testTimeMethodWithIncorrectWarmUpIteration() {
        Timer timer = Timer.newInstance();

        counter = 0;
        thrown.expect(IllegalArgumentException.class);
        timer.warmUp(() -> counter++, -100);
    }
    
    @Test
    public void testTimeMethodWithIncorrectWarmUpAction() {
        Timer timer = Timer.newInstance();

        counter = 0;
        thrown.expect(NullPointerException.class);
        timer.warmUp(null, 100);
    }

    private void sleep(int milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String sleepReturn(int milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "result";
    }
}
