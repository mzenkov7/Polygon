package java8.lazy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LazyStringTest {

    private static final String TEST_STRING = "Test_String";

    private static final String AROUND_STRING = "x";
    private static final String BEFORE_STRING = "Around";
    private static final String AFTER_STRING = "After";

    @Test
    public void shouldEqualsOriginWhenCreateFromString() {
        LazyString lazyString = LazyString.of(TEST_STRING);
        assertEquals(TEST_STRING, lazyString.get());
    }

    @Test
    public void shouldEqualsOriginWhenCreateFromSupplier() {
        LazyString lazyString = LazyString.of(() -> TEST_STRING);
        assertEquals(TEST_STRING, lazyString.get());
    }

    @Test
    public void shouldBeAroundedWhenCreateFromString() {
        String expected = AROUND_STRING + TEST_STRING + AROUND_STRING;

        LazyString lazyString = LazyString.of(TEST_STRING).around(AROUND_STRING);
        assertEquals(expected, lazyString.get());
    }

    @Test
    public void shouldBeAroundedWhenCreateFromSupplier() {
        String expected = AROUND_STRING + TEST_STRING + AROUND_STRING;

        LazyString lazyString = LazyString.of(TEST_STRING).around(() -> AROUND_STRING);
        assertEquals(expected, lazyString.get());
    }

    @Test
    public void shouldBeBeforeAddedWhenCreateFromString() {
        String expected = BEFORE_STRING + TEST_STRING;

        LazyString lazyString = LazyString.of(TEST_STRING).before(BEFORE_STRING);
        assertEquals(expected, lazyString.get());
    }

    @Test
    public void shouldBeBeforeAddedWhenCreateFromSupplier() {
        String expected = BEFORE_STRING + TEST_STRING;

        LazyString lazyString = LazyString.of(TEST_STRING).before(() -> BEFORE_STRING);
        assertEquals(expected, lazyString.get());
    }

    @Test
    public void shouldBeAfterAddedWhenCreateFromString() {
        String expected = TEST_STRING + AFTER_STRING;

        LazyString lazyString = LazyString.of(TEST_STRING).after(AFTER_STRING);
        assertEquals(expected, lazyString.get());
    }

    @Test
    public void shouldBeAfterAddedWhenCreateFromSupplier() {
        String expected = TEST_STRING + AFTER_STRING;

        LazyString lazyString = LazyString.of(TEST_STRING).after(() -> AFTER_STRING);
        assertEquals(expected, lazyString.get());
    }
}
