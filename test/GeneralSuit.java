
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import concurrent.cache.CacheSuit;
import java8.lazy.LazySuit;
import list.ListSuit;
import timer.TimerSuit;

@RunWith(Suite.class)
@SuiteClasses({ LazySuit.class, ListSuit.class, CacheSuit.class, TimerSuit.class })
public class GeneralSuit {
}
