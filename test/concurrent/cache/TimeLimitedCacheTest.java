package concurrent.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TimeLimitedCacheTest {

    private final int LIMIT = 30;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    <T> Cache<T> newLimitedCache() {
        return TimeLimitedCache.newInstance(TimeUnit.SECONDS, 10, TimeUnit.DAYS, 1);
    }
    
    <T> Cache<T> newLimitedCache(int timeToLive, int waitTime) {
        return TimeLimitedCache.newInstance(timeToLive, waitTime);
    }
    
    <T> Cache<T> newLimitedCache(TimeUnit timeToLiveUnit, long timeToLive, TimeUnit waitTimeUnit,
            long waitTime) {
        return TimeLimitedCache.newInstance(timeToLiveUnit, timeToLive, waitTimeUnit, waitTime);
    }
    
    @Test
    public void testThrowExceptionWhenUnsupportedTimeToLiveUnit() {
        thrown.expect(IllegalArgumentException.class);
        newLimitedCache(TimeUnit.NANOSECONDS, 2, TimeUnit.SECONDS, 2);
    }

    @Test
    public void testThrowExceptionWhenUnsupportedWaitTimeUnit() {
        thrown.expect(IllegalArgumentException.class);
        newLimitedCache(TimeUnit.SECONDS, 2, TimeUnit.NANOSECONDS, 2);
    }
    
    @Test
    public void testThrowExceptionWhenUnsupportedTimeUnits() {
        thrown.expect(IllegalArgumentException.class);
        newLimitedCache(TimeUnit.NANOSECONDS, 2, TimeUnit.NANOSECONDS, 2);
    }
    
    @Test
    public void testThrowExceptionWhenNegativeTimeToLive() {
        thrown.expect(IllegalArgumentException.class);
        newLimitedCache(-2, 2);
    }
    
    @Test
    public void testThrowExceptionWhenNegativeWaitTime() {
        thrown.expect(IllegalArgumentException.class);
        newLimitedCache(2, -2);
    }
    
    @Test
    public void testThrowExceptionWhenNegativeTimes() {
        thrown.expect(IllegalArgumentException.class);
        newLimitedCache(-2, -2);
    }

    @Test
    public void testChangeGetValueWhenTimeExpired() throws InterruptedException {
        Cache<String> cache = newLimitedCache(90, 50);
        String key = "key";
        String value = "value";
        cache.put(key, value);
        TimeUnit.MILLISECONDS.sleep(50);
        assertEquals(value, cache.get(key));
        TimeUnit.MILLISECONDS.sleep(50);
        assertNull(cache.get(key));
    }
    
    @Test
    public void testChangeSizeWhenTimeExpired() throws InterruptedException {
        Cache<String> cache = newLimitedCache(90, 50);
        String key = "key";
        String value = "value";
        cache.put(key, value);
        TimeUnit.MILLISECONDS.sleep(50);
        assertEquals(1, cache.size());
        TimeUnit.MILLISECONDS.sleep(50);
        assertEquals(0, cache.size());
    }

    @Test
    public void shouldReturnNullWhenCacheIsEmpty() {
        Cache<String> cache = newLimitedCache();

        String actualValue = cache.get("key");

        assertNull(actualValue);
        assertEquals(cache.size(), 0);
    }

    @Test
    public void shouldReturnValueAfterItHasBeenAdded() {
        Cache<String> cache = newLimitedCache();
        final String EXPECTED_VALUE = "value";
        final String key = "key";

        cache.put(key, EXPECTED_VALUE);
        String actualValue = cache.get(key);

        assertEquals(EXPECTED_VALUE, actualValue);
        assertEquals(cache.size(), 1);
    }

    @Test
    public void shouldReturnToStringRepresentationOfCacheLikeMap() {
        Cache<String> cache = newLimitedCache();
        Map<String, String> map = new HashMap<>();
        final String key = "key";
        final String value = "value";

        cache.put(key, value);
        map.put(key, value);

        assertEquals(map.toString(), cache.toString());
    }

    @Test
    public void shouldBeCorrectSizeWhenConcurrentPut() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        List<Callable<Void>> tasks = new ArrayList<>();

        Cache<String> cache = newLimitedCache();
        final int SIZE = LIMIT * 100;

        for (int i = 1; i <= SIZE; i++) {
            final int id = i;
            tasks.add(() -> {
                cache.put(String.valueOf(id), "String" + id);
                return null;
            });
        }
        executor.invokeAll(tasks);
        executor.shutdown();

        assertEquals(SIZE, cache.size());
    }
}
