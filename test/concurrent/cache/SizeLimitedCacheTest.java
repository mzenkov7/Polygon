package concurrent.cache;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;

public class SizeLimitedCacheTest {

    private final int LIMIT = 16;

    <T> Cache<T> newLimitedCache(int size) {
        return new SizeLimitedCache<>(size);
    }

    @Test
    public void testSizeWhenAddLessThanCacheLimit() {
        Cache<String> cache = newLimitedCache(LIMIT);
        final int SIZE = 10;
        final int EXPECTED_SIZE = 10;

        for (int i = 1; i <= SIZE; i++) {
            String key = String.valueOf(i);
            String value = "String" + i;
            cache.put(key, value);
        }

        assertEquals(EXPECTED_SIZE, cache.size());
    }

    @Test
    public void testSizeWhenAddMoreThanCacheLimit() {
        Cache<String> cache = newLimitedCache(LIMIT);
        final int SIZE = 20;
        final int EXPECTED_SIZE = LIMIT;

        for (int i = 1; i <= SIZE; i++) {
            String key = String.valueOf(i);
            String value = "String" + i;
            cache.put(key, value);
        }

        assertEquals(EXPECTED_SIZE, cache.size());
    }

    @Test
    public void shouldReturnNullWhenCacheIsEmpty() {
        Cache<String> cache = newLimitedCache(LIMIT);
        final String EXPECTED_VALUE = null;

        String actualValue = cache.get("key");

        assertEquals(EXPECTED_VALUE, actualValue);
        assertEquals(cache.size(), 0);
    }

    @Test
    public void shouldReturnValueAfterItHasBeenAdded() {
        Cache<String> cache = newLimitedCache(LIMIT);
        final String EXPECTED_VALUE = "value";
        final String key = "key";

        cache.put(key, EXPECTED_VALUE);
        String actualValue = cache.get(key);

        assertEquals(EXPECTED_VALUE, actualValue);
        assertEquals(cache.size(), 1);
    }

    @Test
    public void shouldReturnToStringRepresentationOfCacheLikeMap() {
        Cache<String> cache = newLimitedCache(LIMIT);
        Map<String, String> map = new HashMap<>();
        final String key = "key";
        final String value = "value";

        cache.put(key, value);
        map.put(key, value);

        assertEquals(map.toString(), cache.toString());
    }

    @Test
    public void shouldBeCorrectSizeWhenConcurrentPut() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        List<Callable<Void>> tasks = new ArrayList<>();

        Cache<String> cache = newLimitedCache(LIMIT);
        final int SIZE = LIMIT * 100;

        for (int i = 1; i <= SIZE; i++) {
            final int id = i;
            tasks.add(() -> {
                cache.put(String.valueOf(id), "String" + id);
                return null;
            });
        }
        executor.invokeAll(tasks);
        executor.shutdown();

        assertEquals(LIMIT, cache.size());
    }
}
