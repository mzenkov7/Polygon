package concurrent.cache;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SizeLimitedCacheTest.class, TimeLimitedCacheTest.class })
public class CacheSuit {
}
