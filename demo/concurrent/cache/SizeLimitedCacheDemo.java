package concurrent.cache;

import java.util.stream.IntStream;

public class SizeLimitedCacheDemo {

    public static void main(String[] args) {
        Cache<String> cache = new SizeLimitedCache<>(20);
        
        IntStream.iterate(1, i -> i + 1).limit(50).parallel().mapToObj(String::valueOf).forEach((k) -> {
            cache.put(k, "String" + k);
            System.out.println("String" + k);
        });

        System.out.println();
        System.out.println("cache.size() = " + cache.size());
        System.out.println("cache = " + cache);
        System.out.println("get(10) = " + cache.get("1"));
        System.out.println("get(20) = " + cache.get("25"));
        System.out.println("get(50) = " + cache.get("50"));
        System.out.println("End");
    }
}
