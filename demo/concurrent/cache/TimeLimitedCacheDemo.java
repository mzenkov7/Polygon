package concurrent.cache;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class TimeLimitedCacheDemo {

    public static void main(String[] args) {
        Cache<String> cache = TimeLimitedCache.newInstance(TimeUnit.SECONDS, 5, TimeUnit.SECONDS, 1);

        IntStream.iterate(1, i -> i + 1).limit(50).mapToObj(String::valueOf).forEach((k) -> {
            sleep(100);
            cache.put(k, "String" + k);
            System.out.println("String" + k);
        });

        while (cache.size() > 0) {
            System.out.println();
            System.out.println("cache.size() = " + cache.size());
            System.out.println("cache = " + cache);
            System.out.println("get(10) = " + cache.get("10"));
            System.out.println("get(20) = " + cache.get("20"));
            System.out.println("get(50) = " + cache.get("50"));
            sleep(500);
        }

        sleep(2000);
        System.out.println();
        System.out.println("cache.size() = " + cache.size());
        System.out.println("cache = " + cache);
        System.out.println("End");
    }

    private static void sleep(int milis) {
        try {
            TimeUnit.MILLISECONDS.sleep(milis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
