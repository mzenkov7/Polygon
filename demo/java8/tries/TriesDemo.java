package java8.tries;

public class TriesDemo {
    public static void main(String[] args) {

        demo1(); // Inheritance mode
        demo2(); // Equals mode
    }

    private static void demo1() {
        Tries<String> tries = new Tries<>();
        String s =
                tries.tries(() -> {
                    return "Success";                               // Ok, print 'Success'
                    // throw new ArrayIndexOutOfBoundsException();  // Ok, expected, then block run; print null
                    // throw new ClassCastException();              // No! ClassCastException is not a child of IndexOutOfBoundsException
                })
                .when(IndexOutOfBoundsException.class)
                .then(e -> System.out.println("Catch ex: " + e))
                // .mode(Tries.Mode.INHERITANCE)                    // by default
                .run();

        System.out.println(s);
    }

    private static void demo2() {

        Tries<String> tries = new Tries<>();
        String s =

                tries.tries(() -> {
                    return "Success";                           // Ok, print 'Success'
                    // throw new Exception();                   // Ok, expected, then block run; print null
                    // throw new IndexOutOfBoundsException();   // Ok, expected, then block run; print null
                    // throw new ClassCastException();          // No! Not expected, RuntimeException throws
                })
                .when(Exception.class)
                .when(IndexOutOfBoundsException.class)
                .mode(Tries.Mode.EQUALS)
                .then(e -> System.out.println("Catch ex: " + e))
                .run();

        System.out.println(s);
    }
}
