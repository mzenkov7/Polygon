package queue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MasterWorkerDemo {

    private static final int WORKERS_COUNT = 3;

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Work> queue = new ArrayBlockingQueue<>(10);

        Master m = new Master(queue);
        new Thread(m).start();
        List<Worker> workers = new ArrayList<>();
        for (int i = 0; i < WORKERS_COUNT; i++) {
            workers.add(new Worker(queue));
        }

        Thread.sleep(5000);

        for (Worker w : workers) {
            new Thread(w).start();
        }
    }
}
