package factory;

public class ValueFactoryDemo {

    public static void main(String[] args) {
        ValueFactory factory1 = ValueFactory.getAudioFactory();
        ValueFactoryLambda factory2 = ValueFactoryLambda.getAudioFactory();
        Value v1 = factory1.create(20, 50, 1);
        Value v2 = factory2.create(2000, 100, 1);

        System.out.println(v1);
        System.out.println(v2);
    }
}
