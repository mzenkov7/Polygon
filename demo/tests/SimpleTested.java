package tests;

import static tests.Assert.*;

import tests.engine.Test;
import tests.engine.TestEngine;

public class SimpleTested {

    public static void main(String[] args) {

        // get test engine and test out class
        TestEngine engine = new TestEngine();
        engine.test(SimpleTested.class);
    }

    @Test
    public void assertAutoBoxingEquals() {
        // valid test because 4 < 5
        assertTrue(new Integer(4) < 5);
    }

    // not valid method, @Test annotation required
    public void notTested() {
    }

    @Test(expected = RuntimeException.class)
    public void notCoughtException() {
        throw new RuntimeException();
    }

    @Test(expected = IllegalArgumentException.class)
    public void expectedException() {
        // throwing of expected exception
        throw new IllegalArgumentException();
    }

    @Test
    public void compareEqualsObjects() {
        // valid test because strings are equals
        assertEquals("First", "First");
    }

    @Test
    public void assertPrimitivesEquals() {
        // valid test, equal primitives
        assertEquals(5, 5);
    }

    @Test
    public void assertBooleanCondition() {
        // valid test
        assertTrue(true);
    }
}
