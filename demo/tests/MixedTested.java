package tests;

import static tests.Assert.*;

import tests.engine.Test;
import tests.engine.TestEngine;

public class MixedTested {

    public static void main(String[] args) {

        // get test engine and test out class
        TestEngine engine = new TestEngine();
        engine.test(MixedTested.class);
    }

    @Test
    public void assertAutoBoxingEquals() {
        // valid test because 4 < 5
        assertTrue(new Integer(4) < 5);
    }

    // not valid method, @Test annotation required
    public void notTested() {
    }

    @Test
    public void notCoughtException() {
        // not valid test, exception is not catching
        throw new RuntimeException();
    }

    @Test(expected = IllegalArgumentException.class)
    public void expectedException() {
        // valid test, throws expected exception
        throw new IllegalArgumentException();
    }

    @Test
    public void compareNotEqualsObjects() {
        // not valid test, different strings
        assertEquals("First", "Second");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void unexpectedException() {
        // not valid test, exception is unexpected
        throw new IllegalStateException();
    }

    @Test
    public void assertPrimitivesEquals() {
        // valid test, equal primitives
        assertEquals(5, 5);
    }

    @Test
    public void assertBooleanNotEquals() {
        // not valid test
        assertTrue(false);
    }
}
