package list;

import java.util.List;

public class CompactListDemo {

    public static void main(String[] args) {
        List<String> list = new CompactList<>("Test", 2);
        for (String s : list) {
            System.out.println(s);
        }
    }
}
