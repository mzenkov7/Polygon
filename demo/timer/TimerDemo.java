package timer;

import java.util.concurrent.TimeUnit;

public class TimerDemo {

    public static void main(String[] args) {
        Timer timer = Timer.newInstance();  // By default, TimeUnit.MILLISECONDS is using.

        // Take action without result.
        TimeSummary t = timer.time(() -> {
            System.out.println("Start work");
            sleep();
            System.out.println("End work");
        });

        // Print time info.
        System.out.println(t.getTime());
        System.out.println(t.getTime(TimeUnit.MICROSECONDS));
        System.out.println(t.getTime(TimeUnit.NANOSECONDS));

        // Take action with result.
        TimeSummaryResult<String> t2 = timer.time(() -> {
            System.out.println("Start work");
            sleep();
            System.out.println("End work");
            return "result";
        });

        // Print result of action execution.
        System.out.println(t2.getResult());
        // Print time info.
        System.out.println(t2.getTime());
        System.out.println(t2.getTime(TimeUnit.MICROSECONDS));
        System.out.println(t2.getTime(TimeUnit.NANOSECONDS));
    }

    private static void sleep() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
